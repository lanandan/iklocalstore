package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.utils.NoInternet;
import com.ikomet.iklocalstore.utils.ServiceHandler;

public class ForgotPasswordSendMail extends Activity{

	EditText email;
	Button sendMail;
	private ProgressDialog pDialog;
	String errors;
	
	 private static final String FORGOTPASS_SENDMAIL_URL = "http://multistore.verifiedwork.com/webservice/forgetpwd.php";
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fp_sendmail);
		
		email = (EditText)findViewById(R.id.ed_mail_id); 
		sendMail = (Button)findViewById(R.id.bt_sendmail);
		
		email.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String regex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(email.getText()
							.toString());
					if (email.getText().length() >= 6 && matcher.matches()) {
						email.setError(null);
					} else {
						email.setError("Invalid Email id");
						/*Toast.makeText(getApplicationContext(),
								"Invalid Email id", 1000).show();*/
					}
				}
			}
		});
		
		sendMail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				String regex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(email.getText()
						.toString());
				if (email.getText().length() >= 6 && matcher.matches()) {
					email.setError(null);
					if(RegisterActivity.isInternetConnected(getApplicationContext())){
					    new ForgetPasswordValidation().execute();
					}else{
						NoInternet.internetConnectionFinder(getApplicationContext());
					}
				} else {
					email.setError("Invalid Email id");
					/*Toast.makeText(getApplicationContext(),
							"Invalid Email id", 1000).show();*/
				}
				
				//Toast.makeText(getApplicationContext(), "Not part of this build", Toast.LENGTH_SHORT).show();
				
			}
		});
		
	}

	class ForgetPasswordValidation extends AsyncTask<String, String, String> {

	  @Override
      protected void onPreExecute() {
          super.onPreExecute();
          pDialog = new ProgressDialog(ForgotPasswordSendMail.this);
          pDialog.setMessage("Checking User...");
          pDialog.setIndeterminate(false);
          pDialog.setCancelable(false);
          pDialog.show();
      }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
          int success;
          
              
          String mail = email.getText().toString();
                  
          try {
              // Building Parameters
              List<NameValuePair> params = new ArrayList<NameValuePair>();
              params.add(new BasicNameValuePair("username", mail));
             // params.add(new BasicNameValuePair("pwd", pword));
             // params.add(new BasicNameValuePair("reg_id", gcm1));

              Log.d("request!", "starting");
              // getting product details by making HTTP request
              ServiceHandler sh = new ServiceHandler();
              
              String json = sh.makeServiceCall(FORGOTPASS_SENDMAIL_URL, 2, params);

              JSONObject jsonObj = new JSONObject(json);
      
              success = Integer.parseInt(jsonObj.getString("code"));
              if (success == 200) {
              	Log.d("Validation Successfull!", "  " );
              	//Toast.makeText(ForgotPasswordSendMail.this, "Mail Sent Successfully", Toast.LENGTH_LONG).show();
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
              	return jsonObj.getString("code");
              }else{
           	   
              	Log.d("Login Failure!", jsonObj.getString("errors"));
              	errors = jsonObj.getString("errors");
              	//Toast.makeText(ForgotPasswordSendMail.this, jsonObj.getString("errors"), Toast.LENGTH_LONG).show();
              	
              	return null;
           	   }
              	
          } catch (JSONException e) {
              e.printStackTrace();
          }

          return null;

		}
		/**
       * After completing background task Dismiss the progress dialog
       * **/
      protected void onPostExecute(String file_url) {
          // dismiss the dialog once product deleted
          pDialog.dismiss();
          if (file_url != null){
        	  Toast.makeText(ForgotPasswordSendMail.this, "Mail Sent Successfully", Toast.LENGTH_SHORT).show();
          }else{
       	      Toast.makeText(ForgotPasswordSendMail.this, errors, Toast.LENGTH_SHORT).show();
          }

      }

	}
	
	
	
}

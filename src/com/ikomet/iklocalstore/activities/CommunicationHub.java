package com.ikomet.iklocalstore.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.adapters.CustomCommnRecyclerAdapter;
import com.ikomet.iklocalstore.bean.ImageBean;

public class CommunicationHub extends Activity {

	protected static final int REQUEST_CAMERA = 0;
	protected static final int SELECT_FILE = 1;
	RecyclerView rycl_comm;
	CustomCommnRecyclerAdapter adapter;
	EditText chat;
	ImageButton send, pick;
	List<Object> items;
	boolean firstMsg;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_communicationhub);

		rycl_comm = (RecyclerView) findViewById(R.id.recyclerView_commn);
		LinearLayoutManager llmanager = new LinearLayoutManager(
				CommunicationHub.this);
		llmanager.setStackFromEnd(true);
		rycl_comm.setLayoutManager(llmanager);
		rycl_comm.setItemAnimator(new DefaultItemAnimator());
		rycl_comm.setHasFixedSize(false);

		chat = (EditText) findViewById(R.id.ed_typo);
		send = (ImageButton) findViewById(R.id.img_send);
		pick = (ImageButton) findViewById(R.id.img_pick);
		items = new ArrayList<Object>();
		firstMsg = true;

		send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String chatTex = chat.getText().toString().trim();

				if (firstMsg) {
					if (!chatTex.equalsIgnoreCase("")) {
						items.add(chatTex);
					}
					adapter = new CustomCommnRecyclerAdapter(
							CommunicationHub.this, items);
					rycl_comm.setAdapter(adapter);
				} else {
					if (!chatTex.equalsIgnoreCase("")) {
						items.add(chatTex);
						adapter.notifyDataSetChanged();
					}
				}

				chat.setText("");

			}
		});

		pick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				selectImage();

			}
		});

	}

	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				CommunicationHub.this);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {

				if (items[item].equals("Take Photo")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_FILE);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
				File destination = new File(
						Environment.getExternalStorageDirectory(),
						System.currentTimeMillis() + ".jpg");
				FileOutputStream fo;
				try {
					destination.createNewFile();
					fo = new FileOutputStream(destination);
					fo.write(bytes.toByteArray());
					fo.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				// ivImage.setImageBitmap(thumbnail);
				items.add(new ImageBean(thumbnail));
				adapter.notifyDataSetChanged();
			} else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();
				String[] projection = { MediaColumns.DATA };
				CursorLoader cursorLoader = new CursorLoader(this,
						selectedImageUri, projection, null, null, null);
				Cursor cursor = cursorLoader.loadInBackground();
				int column_index = cursor
						.getColumnIndexOrThrow(MediaColumns.DATA);
				cursor.moveToFirst();
				String selectedImagePath = cursor.getString(column_index);
				Log.e("test", "" + selectedImagePath);
				Bitmap bm;
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				BitmapFactory.decodeFile(selectedImagePath, options);
				final int REQUIRED_SIZE = 200;
				int scale = 1;
				while (options.outWidth / scale / 2 >= REQUIRED_SIZE
						&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
					scale *= 2;
				options.inSampleSize = scale;
				options.inJustDecodeBounds = false;
				bm = BitmapFactory.decodeFile(selectedImagePath, options);
				// ivImage.setImageBitmap(bm);
				items.add(new ImageBean(bm));
				adapter.notifyDataSetChanged();
			}
		}

	}

}

package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.bean.StoreInformations;
import com.ikomet.iklocalstore.bean.Stores;
import com.ikomet.iklocalstore.utils.InternetConnection;
import com.ikomet.iklocalstore.utils.NoInternet;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;

public class RegisterActivity extends Activity {

	EditText fname, uname, pwd, cnfmpwd, mail, city, phone;
	String regtype = "";
	RadioGroup rggender;
	RadioButton rdmale, rdfemale;
	Button register;
	
	private ProgressDialog pDialog;
	
	// GoogleCloudMessaging gcm;
	Context context;
	
	RelativeLayout rl;
	EditText ed;
	ImageView iv;
	String list_regtype[] = { "customer", "owner" };
	public static String list_store_types[];
		
	private static final String REG_URL = "http://multistore.verifiedwork.com/webservice/register.php";
	private static final String TAG_MESSAGE = "errors";
	String latitude = "", longitude = "";
		
	ArrayList<Category> catDetails;  
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		uname = (EditText) findViewById(R.id.ed_firstname);
		mail = (EditText) findViewById(R.id.ed_mailid);
		phone = (EditText) findViewById(R.id.ed_phone);
		rggender = (RadioGroup) findViewById(R.id.rg_gender);
		rdmale = (RadioButton) findViewById(R.id.rd_male);
		rdfemale = (RadioButton) findViewById(R.id.rd_female);
		pwd = (EditText) findViewById(R.id.ed_pass);
		pwd.setTypeface(Typeface.DEFAULT);
		cnfmpwd = (EditText) findViewById(R.id.ed_conf_pass);
		cnfmpwd.setTypeface(Typeface.DEFAULT);
	
		register = (Button) findViewById(R.id.bt_register);

		rl = (RelativeLayout) findViewById(R.id.rl_st_type);
		ed = (EditText) findViewById(R.id.ed_sel_st_type);
		iv = (ImageView) findViewById(R.id.img_sel_st_type);
		
		uname.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {

					if (uname.getText().length() >= 6
							&& uname.getText().length() <= 15) {
						uname.setError(null);
					} else if (uname.getText().length() > 15) {
						uname.setError("Username should not exceed 15 characters");
					} else {
						uname.setError("Username should be minimum 6 characters");
					}
				}
			}
		});

		phone.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {

					if (phone.getText().length() < 10) {
						phone.setError("Mobile number must be minimum 10 digits");
					} else {
						phone.setError(null);
					}
				}
			}
		});

		mail.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String regex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern
							.matcher(mail.getText().toString());
					if (mail.getText().length() >= 4 && matcher.matches()) {
						mail.setError(null);
					} else {
						mail.setError("Invalid Email id");
						/*
						 * Toast.makeText(getApplicationContext(),
						 * "Invalid Email id", 1000).show();
						 */
					}
				}
			}
		});

		pwd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {

					if (pwd.getText().length() >= 6
							&& pwd.getText().length() <= 15) {
						pwd.setError(null);
					} else if (pwd.getText().length() > 15) {
						pwd.setError("Password should be maximum 15 characters");
					} else {
						pwd.setError("Password should be minimum 6 characters");
					}
				}
			}
		});

		cnfmpwd.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {

					if (pwd.getText().toString()
							.equals(cnfmpwd.getText().toString())) {
						cnfmpwd.setError(null);
					} else {
						cnfmpwd.setError("Password doesn't match");
					}
				}
			}
		});

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (regtype.equals("owner")) {
					showToast("Registration for owner is not currently available");
				} else {

					if (registrationvalidation()) {

						Currentlocation cl = new Currentlocation(getApplicationContext());
						
						if(InternetConnection.isInternetConnected(getApplicationContext()) && cl.canGetLocation()){
							
							String lat,lon;
							lat = String.valueOf(cl.getLatitude());
							lon = String.valueOf(cl.getLatitude());
							
							new CreateUser(lat, lon).execute();
							
						}else if(cl.canGetLocation()){
							NoInternet.internetConnectionFinder(getApplicationContext());
						}else{
							ShowToasts.showToast(getApplicationContext(), "Please enable GPS");
						}
						
						
					} else {
						ShowToasts.showToast(getApplicationContext(), "Please fill the form correctly");
					}
				}
			}
		});

	}

	public void showPopup(View v) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose registration type");

		builder.setItems(list_regtype, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				if(item==1){
					ShowToasts.showToast(getApplicationContext(), "Registeration as owner is currently not available");
				}
				regtype = list_regtype[0];
				ed.setText(list_regtype[0]);
			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	class CreateUser extends AsyncTask<String, String, String> {

		String lat,lon;
		
		public CreateUser(String lat, String lon){
			this.lat = lat;
			this.lon = lon;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RegisterActivity.this);
			pDialog.setMessage("Registering...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
			ServiceHandler sh = new ServiceHandler();
           //String firstname = fname.getText().toString();
           String username = uname.getText().toString();
           String gendername;
           if(rdmale.isChecked()==true){
        	   gendername = "male";
           }else{
        	   gendername = "female";
           }
            
           String password = pwd.getText().toString();
           //String cnfmpassword = cnfmpwd.getText().toString();
           String mailid = mail.getText().toString();
           //String cityname = city.getText().toString();
           String mobile = phone.getText().toString();
           
           
           try {

               List<NameValuePair> params = new ArrayList<NameValuePair>();
               params.add(new BasicNameValuePair("firstname", username));
               params.add(new BasicNameValuePair("phone", mobile));
               params.add(new BasicNameValuePair("email", mailid));
               params.add(new BasicNameValuePair("password", password));
               params.add(new BasicNameValuePair("gender", gendername));
               params.add(new BasicNameValuePair("typeofreg", regtype));
               params.add(new BasicNameValuePair("latitude", lat));
               params.add(new BasicNameValuePair("longtitude", lon));
               
                   
              for(int i=0;i<params.size();i++){
               Log.e("request!", "starting"+params.get(i).toString());
              }
                          
               String jsonStr = sh.makeServiceCall(REG_URL, 2, params);
               
               JSONObject json = new JSONObject(jsonStr);

               Log.e("Registration Attempt", json.toString());

               int code = Integer.parseInt(json.getString("code"));//||Integer.parseInt(json.getString("code1")));
               
               if (code == 200) {
               	Log.e("User Created!", json.toString());
//               	st = new ArrayList<StoreInformations>();
               	
            	String unm = uname.getText().toString().trim();
               	String token = json.getString("token");
               	String email = json.getString("username");
               	
               	SharedPrefs.writeSP(RegisterActivity.this, unm, email, token, password);
               	
                JSONArray jar = json.getJSONArray("category");
                
                catDetails = new ArrayList<Category>();
                
                for(int i=0;i<jar.length();i++){
                	
                JSONObject jobj_cat = jar.getJSONObject(i);	
                	
                String name = jobj_cat.getString("name");
                String image = jobj_cat.getString("image");
                
                JSONArray jar_stores = jobj_cat.getJSONArray("stores");

            	ArrayList<Stores> storeDetails = new ArrayList<Stores>();
               	
               	for(int j=0;j<jar_stores.length();j++){
               
               		JSONObject jobj_stores = jar_stores.getJSONObject(j);
               		
               		String storeid = jobj_stores.getString("storeid");
               		String storename = jobj_stores.getString("storename");
               		String latitude = jobj_stores.getString("latitude");
               		String longitude = jobj_stores.getString("longitude");
               		String storetype = jobj_stores.getString("storetype");
               		String storelogo = jobj_stores.getString("storelogo");
               		
               		storeDetails.add(new Stores(storeid, storename, latitude, longitude, storetype, storelogo));
               		
//               	st.add(new StoreInformations(storeid, storename, latitude, longitude, storetype));
               	
               	}
               	catDetails.add(new Category(name, image, storeDetails));
               	
               	
               }
                
                return json.getString("msg");
                
               }
                
                else{
               	Log.e("Login Failure!", json.getString(TAG_MESSAGE));
               	return null;

               }
           } catch (JSONException e) {
               e.printStackTrace();
           }

           return null;

		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();
			if (file_url!=null) {
				ShowToasts.showToast(getApplicationContext(), " "+file_url);
				finish();
               	overridePendingTransition(R.anim.right_in, R.anim.left_out);
				Intent intent = new Intent(RegisterActivity.this, MapActivity.class);
				intent.putExtra("StoreDetails", catDetails);
				startActivity(intent);
			}else{
				
			}
			
			
		}

	}

	public boolean registrationvalidation() {

		if (regtype.equals("")) {
			showToast("select registration type");
			return false;
		}
		
		if (uname.getText().toString().trim().equalsIgnoreCase("")
				|| uname.getText().toString().length() < 6
				|| uname.getText().toString().length() > 15
				|| mail.getText().toString().trim().equalsIgnoreCase("")
				|| pwd.getText().toString().trim().equalsIgnoreCase("")
				|| pwd.getText().toString().length() < 6
				|| pwd.getText().toString().length() > 15
				|| cnfmpwd.getText().toString().trim().equalsIgnoreCase("")
				|| !cnfmpwd.getText().toString().trim()
						.equals(pwd.getText().toString().trim())
				|| phone.getText().toString().trim().equalsIgnoreCase("")
				|| phone.getText().toString().length() < 10) {

			if (uname.getText().toString().trim().equalsIgnoreCase("")
					|| uname.getText().toString().length() < 6
					|| uname.getText().toString().length() > 15) {
				if (uname.getText().toString().trim().equalsIgnoreCase("")) {
					showToast("Username must not be blank");
				} else if (uname.getText().toString().length() < 6) {
					showToast("username must be minimum 6 characters");
				} else {
					showToast("username must not exceed 15 characters");
				}
			}
			if (pwd.getText().toString().trim().equalsIgnoreCase("")
					|| pwd.getText().toString().length() < 6
					|| pwd.getText().toString().length() > 15) {
				if (pwd.getText().toString().trim().equalsIgnoreCase("")) {
					showToast("Password must not be blank");
				} else if (pwd.getText().toString().length() < 6) {
					showToast("Password must be minimum 6 characters");
				} else {
					showToast("Password must not exceed 15 characters");
				}
			}
			if (cnfmpwd.getText().toString().trim().equalsIgnoreCase("")
					|| !cnfmpwd.getText().toString().trim()
							.equals(pwd.getText().toString().trim())) {
				if (cnfmpwd.getText().toString().trim().equalsIgnoreCase("")) {
					showToast("Confirm password must not be blank");
				} else {
					showToast("Passwords does not match");
				}
			}
			if (mail.getText().toString().trim().equalsIgnoreCase("")) {
				showToast("Invalid e-mail");

			}
			if (phone.getText().toString().trim().equalsIgnoreCase("")
					|| phone.getText().toString().trim().length() < 10) {

				showToast("Invalid phone number");

			}
			if (!(pwd.getText().toString().trim().equals(cnfmpwd.getText()
					.toString().trim()))) {
				showToast("Password Mismatch");
			}
			if (pwd.getText().toString().trim().equalsIgnoreCase("")) {
				// showToast("Invalid password");

			}
			if (cnfmpwd.getText().toString().trim().equalsIgnoreCase("")) {
				// showToast("Invalid password");

			}

			showToast("Please Fill all the fields correctly");
			return false;
		} else {

			return true;

		}

	}

	public static boolean isInternetConnected(Context ctx) {
		ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		if (wifi != null) {
			if (wifi.isConnected()) {
				return true;
			}
		}
		if (mobile != null) {
			if (mobile.isConnected()) {
				return true;
			}
		}
		return false;
	}

	private void showToast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}

}

package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;
import java.util.Stack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.adapters.ListViewAdapter;
import com.ikomet.iklocalstore.bean.ProductCategory;
import com.ikomet.iklocalstore.bean.ProductStore;
import com.ikomet.iklocalstore.fragments.FragmentDrawerNew;
import com.ikomet.iklocalstore.fragments.FragmentMainScreen;

public class MainScreenActivity extends ActionBarActivity{
	
	private Toolbar mToolbar;
	ListViewAdapter lva;
	private FragmentDrawerNew drawerFragmentNew;
	Fragment frag = null;
	ArrayList<ProductCategory> prodCat;
	ProductStore prodStore;
	
	public static Stack<Fragment> fragmentStack;
	public static FragmentManager fragmentManager;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mainscreen);
		
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
          
        fragmentStack = new Stack<Fragment>();
        fragmentManager = getSupportFragmentManager();
        
        prodCat = new ArrayList<ProductCategory>();
        prodCat = (ArrayList<ProductCategory>) getIntent().getSerializableExtra("productCat");
        prodStore = (ProductStore) getIntent().getSerializableExtra("prodStore");
        
        drawerFragmentNew = (FragmentDrawerNew)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer_exp);
        
        drawerFragmentNew.getProductCatDet(prodCat, prodStore);
        drawerFragmentNew.setUp(R.id.fragment_navigation_drawer_exp, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        
        if(frag==null){
            
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            frag = new FragmentMainScreen(prodCat, prodStore);
            fragmentTransaction.add(R.id.container_body, frag);
            fragmentStack.push(frag);
            fragmentTransaction.commit();
            
            }
        
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_listofitems, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.item_search) {
			Intent intent = new Intent(MainScreenActivity.this, SearchItemsActivity.class);
			startActivity(intent);
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (fragmentStack.size() == 2) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			fragmentStack.lastElement().onPause();
			ft.remove(fragmentStack.pop());
			fragmentStack.lastElement().onResume();
			ft.show(fragmentStack.lastElement());
			ft.commit();
		} else {
			super.onBackPressed();
		}
		
	}
		
}

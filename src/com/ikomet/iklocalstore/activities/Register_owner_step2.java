package com.ikomet.iklocalstore.activities;
/*package com.ikomet.ikshop;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ikomet.ikshop.utils.ServiceHandler;

public class Register_owner_step2 extends Activity{

	EditText st_phone,st_name,st_lat,st_long,pass,rt_pass;
	RelativeLayout st_type;
	EditText ed_st_type;
	ImageView iv_st_type;
	RadioGroup rggender;
	RadioButton rdmale,rdfemale;
	Button register;
	JSONArray ja=null;
	JSONObject jo=null;
	 // Progress Dialog
    private ProgressDialog pDialog;
    Spinner reg_type;
    String gender;
    int code;
    //GoogleCloudMessaging gcm;
	Context context;
	String regId, gcmid;

	RelativeLayout rl;
	EditText ed;
	ImageView iv;
	//String list_regtype[] = {"Customer","Owner"};
		
	public static final String REG_ID = "regId";
	private static final String APP_VERSION = "appVersion";
	static final String TAG = "Register Activity";
	String gcm1;

    private static final String REG_URL = "http://multistore.verifiedwork.com/webservice/register.php";

    //ids
    private static final String TAG_SUCCESS = "code";
    private static final String TAG_MESSAGE = "errors";
    private static final String TAG_USER = "users";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reg_cust_step1);
		
		st_type = (RelativeLayout)findViewById(R.id.rl_st_type);
		ed_st_type = (EditText)findViewById(R.id.ed_sel_st_type);
		iv_st_type = (ImageView)findViewById(R.id.img_sel_st_type);
		
		st_phone = (EditText)findViewById(R.id.ed_st_phone);
		st_name = (EditText)findViewById(R.id.ed_st_name);
		//uname = (EditText)findViewById(R.id.ed_username);
		st_lat = (EditText)findViewById(R.id.ed_st_lat);
		st_long = (EditText)findViewById(R.id.ed_st_long);
		rdmale = (RadioButton)findViewById(R.id.rd_male);
		rdfemale = (RadioButton)findViewById(R.id.rd_female);
		pwd = (EditText)findViewById(R.id.ed_st_phone);
		pwd.setTypeface(Typeface.DEFAULT);
		cnfmpwd = (EditText)findViewById(R.id.ed_conf_pass);
		cnfmpwd.setTypeface(Typeface.DEFAULT);
		//rdmale.setChecked(true);
		rdmale.setText("Male");
		rdmale.setText("Female");
		register = (Button)findViewById(R.id.bt_register);
		
		//reg_type = (Spinner)findViewById(R.id.sp_regtype);
	
		 // Spinner Drop down elements
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Customer");
        categories.add("Owner");
         
        // Creating adapter for spinner
        //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
 
        // Drop down layout style - list view with radio button
       // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 
        // attaching data adapter to spinner
        //reg_type.setAdapter(dataAdapter);
		
		{"id":32,"username":"ganesh.s.mech@gmail.com",
			"token":"37A3392B-57C0-709B-4B3C-E4A125182391","code":200,"msg":"Successfully inserted"}
		reg_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
								
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(registrationvalidation()){
					
				if(CategoriesActivity.isInternetConnected(RegisterActivity.this)){	
					new CreateUser().execute();
				}else{
					
					NoInternet.internetConnectionFinder(RegisterActivity.this);
				}
				}else{
					
				}
				}
		});
			
	}
	
	public void showPopup(View v) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose Store type");

		builder.setItems(list_regtype, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Do something with the selection
				int a = Integer.valueOf(item);
				ed.setHint(list_regtype[a]);

			}
		});

		AlertDialog alert = builder.create();
		alert.show();

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	class CreateUser extends AsyncTask<String, String, String> {

		 *//**
        * Before starting background thread Show Progress Dialog
        * *//*
		boolean failure = false;

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           pDialog = new ProgressDialog(RegisterActivity.this);
           pDialog.setMessage("Registering...");
           pDialog.setIndeterminate(false);
           pDialog.setCancelable(false);
           pDialog.show();
       }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
           
           //String firstname = fname.getText().toString();
           String username = uname.getText().toString();
           String gendername;
           if(rdmale.isChecked()==true){
        	   gendername = rdmale.getText().toString();
           }else{
        	   gendername = rdfemale.getText().toString();
           }
            
           String password = pwd.getText().toString();
           //String cnfmpassword = cnfmpwd.getText().toString();
           String mailid = mail.getText().toString();
           //String cityname = city.getText().toString();
           String mobile = phone.getText().toString();
           
           gcm = GoogleCloudMessaging.getInstance(RegisterActivity.this);
           if (gcm == null) {
				gcm = GoogleCloudMessaging.getInstance(context);
			}
           String gcmRegId="";
		try {
			//gcmRegId = gcm.register(Config.GOOGLE_PROJECT_ID);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
           
           
           
           //String gcmRegId = gcmid;
          // Log.e("GCM", "Registration Id = "+ gcmRegId);
          // showToast("Registration Id = "+ gcmRegId);
           try {
               // Building Parameters
        	  // fnm,gender,pwd,mail,contact
        	   //firstname, phone , email, password
               List<NameValuePair> params = new ArrayList<NameValuePair>();
               params.add(new BasicNameValuePair("firstname", username));
               //params.add(new BasicNameValuePair("unm", username));
               //params.add(new BasicNameValuePair("gender", gendername));
               params.add(new BasicNameValuePair("password", password));
               //params.add(new BasicNameValuePair("cpwd", cnfmpassword));
               params.add(new BasicNameValuePair("email", mailid));
               params.add(new BasicNameValuePair("phone", mobile));
              // params.add(new BasicNameValuePair("reg_id", gcmRegId));
               
               Log.e("request!", "starting");

               //Posting user data to script
               JSONObject json = jsonParser.makeHttpRequest(
                      LOGIN_URL, "POST", params);
           
               ServiceHandler sh = new ServiceHandler();
               sh.makeServiceCall(REG_URL, 2, params);
             //Posting user data to script
               
               String jsonStr = sh.makeServiceCall(REG_URL, 2, params);
               
               JSONObject json = new JSONObject(jsonStr);
               
               JSONObject json = jsonParser.makeHttpRequest(
                      REG_URL, "POST", params);
               //Log.e("Login attempt", json);
               // full json response
               Log.e("Login attempt", json.toString());

               jo = new JSONObject(json);
               ja = jo.getJSONArray("errors");
               
               // json success element
               code = Integer.parseInt(json.getString("code"));
               if (code == 200) {
               	Log.e("User Created!", json.toString());
               	
               	LoginActivity.prefLogin=getSharedPreferences(LoginActivity.MyPREFERENCES,Context.MODE_PRIVATE);
	           	 Editor editor = LoginActivity.prefLogin.edit();
	           	 editor.putString("UNAME", username);
	             editor.commit();
	            LoginActivity.storedUsrnm = LoginActivity.prefLogin.getString("UNAME", "");                	
               	RegisterActivity.this.finish();
               	overridePendingTransition(R.anim.right_in, R.anim.left_out);
               	return json.getString(TAG_MESSAGE);
               }else{
               	Log.e("Login Failure!", json.getString(TAG_MESSAGE));
               	return null;

               }
           } catch (JSONException e) {
               e.printStackTrace();
           }

           return null;

		}
		*//**
        * After completing background task Dismiss the progress dialog
        * **//*
       protected void onPostExecute(String file_url) {
           // dismiss the dialog once product deleted
           pDialog.dismiss();
           if(code==200){
        	   Toast.makeText(RegisterActivity.this, file_url, Toast.LENGTH_LONG).show();   
           Intent intent = new Intent(RegisterActivity.this, MapActivity.class);
           startActivity(intent);
           finish();
           }
           if (file_url != null){
           	//Toast.makeText(RegisterActivity.this, file_url, Toast.LENGTH_LONG).show();
           }
           
           LoginActivity.prefLogin = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
           if(!LoginActivity.prefLogin.getString("UNAME", "").equalsIgnoreCase("")){
        	   
           }
       }

	}
		
	public boolean registrationvalidation()
	{
		
		String username = uname.getText().toString().trim();
		//String gender;
		
		String password=pwd.getText().toString().trim();
		String cpwd=cnfmpwd.toString();
		String mail_id=mail.getText().toString().trim();
		String mobile_no = phone.getText().toString().trim();
		//String gcmregid = ;
		
		
		if (uname.getText().toString().trim().equalsIgnoreCase("") || mail.getText().toString().trim().equalsIgnoreCase("")
				|| pwd.getText().toString().trim().equalsIgnoreCase("")
				|| cnfmpwd.getText().toString().trim().equalsIgnoreCase("")
				|| phone.getText().toString().trim().equalsIgnoreCase("") 
				|| !(pwd.getText().toString().trim().equals(cnfmpwd.getText().toString().trim())))
		{
						
			if(uname.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid username");
				
			}
			if(pwd.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid password");
				
			}
			if(cnfmpwd.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid password");
				
			}
			if(mail.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid e-mail");
				
			}
			if(phone.getText().toString().trim().equalsIgnoreCase(""))
			{
				showToast("Invalid phone number");
				
			}
			if(!(pwd.getText().toString().trim().equals(cnfmpwd.getText().toString().trim())))
			{
				showToast("Password Mismatch");
			}
			
			showToast("Please Fill all the fields");
			return false;
		}
		else
		{
			
			if (pwd.length() > 6)
			{
				if (pwd.length() > 6)
					return true;
				else
				{
					showToast("Password Should be Minimum 6 Digits");
					pwd.setText("");		
					pwd.setError("Password Should be Minimum 6 Digits");
					pwd.requestFocus();
					return false;
				}
			}				
			{
				showToast("Invalid Mobile Number");
				//mobile_no.setText("");
				//mobile_no.setError("Invalid Mobile Number",dr);
				//mobile_no.requestFocus();
				return false;
			}
			return true;
		}
		
		
	}
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
	
	
	
}
*/
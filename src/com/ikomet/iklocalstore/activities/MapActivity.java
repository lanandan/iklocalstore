package com.ikomet.iklocalstore.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.bean.Stores;
import com.ikomet.iklocalstore.fragments.FragmentDrawer;
import com.ikomet.iklocalstore.fragments.FragmentMap;
import com.ikomet.iklocalstore.interfaces.FragmentCommunicator;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;

public class MapActivity extends ActionBarActivity {

	private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;

    AutoCompleteTextView actv;
	ImageView search;
    
    ProgressDialog pDialog;

    public static final String MAP_URL = "http://multistore.verifiedwork.com/webservice/storesbytype.php";
	
    Currentlocation cl;
    
    FragmentMap frag = null;
    
    public static ArrayList<String> exp_title;
   	public static HashMap<String, ArrayList<String>> exp_store_items;
   	
   	String url = "http://multistore.verifiedwork.com/webservice/category.php";
   	 
   	public static Bitmap ar_viewflip[],ar_horscroll[];
   	public static String cat_name[];
   	
   	public static int fab=00;

   	ArrayList<Category> storeDetails;
   	String[] storeName;
    public FragmentCommunicator communicator;
    ArrayAdapter<String> adapter;
    ArrayList<String> storeNames;
    
   	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
				
       setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        
        storeDetails = (ArrayList<Category>)getIntent().getSerializableExtra("StoreDetails");
        
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        
       drawerFragment.getStoreDetails(storeDetails);
       
       if(frag==null){
		FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        frag = new FragmentMap(storeDetails);
        fragmentTransaction.add(R.id.container_body, frag);
        fragmentTransaction.commit();
	}
       
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
		
		actv = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);  
		
        actv.setThreshold(1);//will start working from first character  
        System.out.println(actv.getText().toString());//setting the adapter data into the AutoCompleteTextView  
        actv.setTextColor(getResources().getColor(R.color.colorPrimary));  
        searchStoreInit();
        search = (ImageView)findViewById(R.id.img_search);
            
        search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String storeName = actv.getText().toString();
				
				// This loop checks whether the store name entered by the user is
				// present in the available list. And if its present the searchStore method
				// is called which clears all the map pointers and displays the pointer
				// of the selected store.
				for(int i=0;i<storeNames.size();i++){
				
					if(storeName.equalsIgnoreCase(storeNames.get(i))){
						
						frag.searchStore(storeName);
						break;
					}
					
					if(i==storeNames.size()-1){
						ShowToasts.showToast(getApplicationContext(), "No stores found matching your search");
					}
					
				}
				
			}
		});
        
	}
    
    public void searchStoreInit(){
    	
    	storeNames = new ArrayList<String>();
    	
    	for(int i=0;i<storeDetails.size();i++){
    		for(int j=0;j<storeDetails.get(i).getStoreDetails().size();j++){
    			//Log.e("error_list", "hai "+storeDetails.get(i).getStoreDetails().get(j).getStorename());
    			storeNames.add(storeDetails.get(i).getStoreDetails().get(j).getStorename());
    				
    		}
    	}
    	
    	storeName = new String[storeNames.size()];
    	for(int i=0;i<storeNames.size();i++){
    		storeName[i] = storeNames.get(i);
    	}
    	
    	adapter = new ArrayAdapter<String>  
        (this,android.R.layout.select_dialog_item,storeName);
    	
    	actv.setAdapter(adapter);
    	
    }
    
	@Override
	protected void onResume() {
		super.onResume();
		
	}

	public Location getLoc(){
		
		return cl.getLocation();
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.log_out) {
			SharedPrefs.clearSP(MapActivity.this);
			System.exit(0);
			return true;
		}else if(id == R.id.recent_stores){
			//showrecentStores();			
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
//		finish();
//		FragmentMap.googleMap.clear();
		System.exit(0);
	}
	
}

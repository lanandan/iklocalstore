package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.bean.Stores;
import com.ikomet.iklocalstore.utils.InternetConnection;
import com.ikomet.iklocalstore.utils.NoInternet;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;

public class LoginActivity extends Activity {

	EditText email, password;
	Button login;
	TextView register, forgot_pass;
	
	private ProgressDialog pDialog;
	
	String user_name;
   	public static String storedUsrnm;

	private static final String LOGIN_URL = "http://multistore.verifiedwork.com/webservice/logincheck.php";
 
	String username_sp, pass_sp, token_sp, mail_sp;
	public String success_msg;
	String storeid=null;
    
	ArrayList<Category> categoryDetails;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
        email = (EditText) findViewById(R.id.ed_username);
		password = (EditText) findViewById(R.id.ed_mailid);
		forgot_pass = (TextView) findViewById(R.id.txt_forgot_pass);
         
		email.setText("visalatchis.m@gmail.com");
		password.setText("gggggg");

		password.setTypeface(Typeface.DEFAULT);
			 
		login = (Button) findViewById(R.id.bt_login);
		register = (TextView) findViewById(R.id.txt_register);

		email.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String regex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(email.getText()
							.toString());
					if (email.getText().length() >= 6 && matcher.matches()) {
						email.setError(null);
					} else {
						email.setError("Invalid Email id");
						
						  Toast.makeText(getApplicationContext(),
						 "Invalid Email id", 1000).show();
						 
					}
				}
			}
		});

		password.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {

					if (password.getText().length() >= 6
							&& password.getText().length() <= 15) {
						password.setError(null);
					} else if (password.getText().length() > 15) {
						password.setError("Password should be maximum 15 characters");
					} else {
						password.setError("Password should be minimum 6 characters");
					}
				}
			}
		});

		register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(InternetConnection.isInternetConnected(getApplicationContext())){
				
				Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
				}else{
					NoInternet.internetConnectionFinder(LoginActivity.this);
				}
			}
		});

		forgot_pass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(LoginActivity.this,
						ForgotPasswordSendMail.class);
				startActivity(intent);
			}
		});

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (loginValidation()) {
					 Currentlocation cl = new Currentlocation(getApplicationContext());
					if(RegisterActivity.isInternetConnected(LoginActivity.this) && cl.canGetLocation){
						String lat,lon;
						lat = String.valueOf(cl.getLatitude());
						lon = String.valueOf(cl.getLongitude());
						new AttemptLogin(lat, lon).execute();
						}else{
							
							if(!InternetConnection.isInternetConnected(getApplicationContext())){
								NoInternet.internetConnectionFinder(LoginActivity.this);
							}else if(!cl.canGetLocation()){
								Toast.makeText(LoginActivity.this, "Please enable GPS", 1000).show();
							}else{
								Toast.makeText(LoginActivity.this, "Please enable GPS & Internet", 1000).show();
							}
							
						}
					}
			}
		});
	}

	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}



	class AttemptLogin extends AsyncTask<String, String, String> {

		String lat,lon;
		
		AttemptLogin(String lat, String lon){
			this.lat = lat;
			this.lon = lon;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
           int success;
          
           String mail = email.getText().toString().trim();
           String pword = password.getText().toString().trim();
           
           
         try {
        	 List<NameValuePair> params = new ArrayList<NameValuePair>();
        	
               params.add(new BasicNameValuePair("username", mail));
               params.add(new BasicNameValuePair("password", pword));
               params.add(new BasicNameValuePair("latitude", lat));
               params.add(new BasicNameValuePair("longitude", lon));
              // params.add(new BasicNameValuePair("reg_id", gcm1));
                      
               // getting product details by making HTTP request
               ServiceHandler sh = new ServiceHandler();
               for(int i=0;i<params.size();i++){
               Log.e("params", params.get(i).toString());
               }
               String json = sh.makeServiceCall(LOGIN_URL, 2, params);
               Log.e("request!", "starting : JSONResult is "+ json);
               
               if(json!=null)
               { 
               JSONObject jsonObj = new JSONObject(json);
          
               // json success tag
               success = Integer.parseInt(jsonObj.getString("code"));
               if (success == 200) {
               	Log.d("Login Successful!", "  " );
               	
               	categoryDetails = new ArrayList<Category>();
               	
               	String tkn = jsonObj.getString("token");
               	String fnm = jsonObj.getString("firstname");

               	if(!SharedPrefs.canGetSPData(getApplicationContext())){
               	SharedPrefs.writeSP(getApplicationContext(), fnm, mail, tkn, pword);
               	
               	}
               	
               	SharedPrefs.changeToken(getApplicationContext(), tkn);
               	
               	JSONArray jar = jsonObj.getJSONArray("category");
               	
               	               	
               	for(int i=0;i<jar.length();i++){
               		
               		JSONObject jobj1 = jar.getJSONObject(i);
               		String cat_nm = jobj1.getString("name");
               	
               		String cat_image = jobj1.getString("image");
               		
               		JSONArray str_nm = jobj1.getJSONArray("stores");
               		
               		ArrayList<Stores> storeDetails = new ArrayList<Stores>();
               		for(int j=0;j<str_nm.length();j++){
                       	
                   		JSONObject jobj = str_nm.getJSONObject(j);
                   		
                   String storeid = jobj.getString("storeid");
                   		System.out.println(storeid);
                   		String storename = jobj.getString("storename");
                   		String latitude = jobj.getString("latitude");
                   		String longitude = jobj.getString("longitude");
                   		String storetype = jobj.getString("storetype");
                   		String storelogo = jobj.getString("storelogo");
                   		
                   		storeDetails.add(new Stores(storeid, storename, latitude, longitude, storetype, storelogo));
                   		
                   
                   	
                   	}
               		categoryDetails.add(new Category(cat_nm, cat_image, storeDetails));
               	}
             
                return jsonObj.getString("msg");
               }else{
            	   
               	Log.d("Login Failure!", jsonObj.getString("msg"));
               	return null;
            	   }
               }
               else
               {
            	 return null; // Toast.makeText(getApplicationContext(), "Problem with loging ", 1000).show();
            	
               }
                            
           } catch (JSONException e) {
               e.printStackTrace();
           }

           return null;

		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			if(pDialog.isShowing()){
			 pDialog.dismiss();
			}
			if (file_url != null) {
				ShowToasts.showToast(getApplicationContext(), " "+file_url);
				finish();
               	overridePendingTransition(R.anim.right_in, R.anim.left_out);
				Intent intent = new Intent(LoginActivity.this, MapActivity.class);
				intent.putExtra("StoreDetails", categoryDetails);
				startActivity(intent);
			}else {
				Toast.makeText(getApplicationContext(),
						"Problem Logging in", Toast.LENGTH_LONG)
						.show();
			}

		}

	}
	
	public boolean loginValidation() {
		Log.e("Login Validation", "msg");

		if (email.getText().toString().equalsIgnoreCase("")
				|| password.getText().toString().equalsIgnoreCase("")
				|| password.getText().toString().length() < 6
				|| password.getText().toString().length() > 15) {
			if (email.getText().toString().trim().equalsIgnoreCase("")) {
				email.setError("Username should be filled");
			}
			if (password.getText().toString().trim().equalsIgnoreCase("")) {
				password.setError("Password should be filled");
			}
			if (password.getText().toString().length() < 6) {
				password.setError("Password should be minimum 6 characters long");
			}
			if (password.getText().toString().length() > 15) {
				password.setError("Password should not exceed 15 characters");
			}
			return false;
			
		} else {
			email.setError(null);
			password.setError(null);
			return true;

		}

	}

	private void showToast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}

	
}

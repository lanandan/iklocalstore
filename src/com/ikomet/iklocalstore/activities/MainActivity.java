package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.ikomet.iklocalstore.R;

public class MainActivity extends Activity {
	GoogleMap googlemap;
	TextView txt;
	Polyline line, line1;
	Marker hamburg, kodampakkam, subdarcolony;
	static final LatLng subdar = new LatLng(13.05582, 80.227252);
	static final LatLng Nungampakkam = new LatLng(13.059537, 80.242479);
	static final LatLng Kodampakkam = new LatLng(13.055125, 80.222121);

	EditText edt_searchtext;
	Button search_btn;

	ArrayList<String> lat, lng;
	ArrayList<String> name_of_title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lat = new ArrayList<String>();
		lng = new ArrayList<String>();
		name_of_title = new ArrayList<String>();
		googlemap = ((MapFragment) getFragmentManager().findFragmentById(
				R.id.map)).getMap();
		// googlemap.setMyLocationEnabled(true);
		if (googlemap != null) {
			hamburg = googlemap.addMarker(new MarkerOptions()
					.position(Nungampakkam)
					.title("Hamburg")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.localshop)));
			kodampakkam = googlemap.addMarker(new MarkerOptions()
					.position(Kodampakkam)
					.title("kodampakkam")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.localshop)));
			googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(
					Nungampakkam, 15));
			// Zoom in, animating the camera.
			googlemap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
		}
	}
}

package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.utils.NoInternet;
import com.ikomet.iklocalstore.utils.ServiceHandler;

public class ForgotPasswordActivity extends Activity{

	EditText mail,new_pass, confirm_new_pass;
	Toolbar mToolbar;
	Button confirm;
	String mail_url_id,mail_url_uid;
	String errors;
	
	private ProgressDialog pDialog;
	
	 private static final String FORGOTPASS_URL = "http://multistore.verifiedwork.com/webservice/changepassword.php";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgetpassword);
		
		confirm = (Button)findViewById(R.id.bt_sendmail);
		
		//mail = (EditText)findViewById(R.id.ed_fp_email);
		new_pass = (EditText)findViewById(R.id.ed_mail_id);
		confirm_new_pass = (EditText)findViewById(R.id.ed_fp_confirmpassword);
		new_pass.setTypeface(Typeface.DEFAULT);
		confirm_new_pass.setTypeface(Typeface.DEFAULT);
		
		/*mToolbar = (Toolbar)findViewById(R.id.toolbar);
		
		setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Forget Password");*/
		
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        mail_url_id = data.toString();
        mail_url_uid = data.toString();
        mail_url_id = mail_url_id.replace("http://multistore.verifiedwork.com/webservice/changepwd.php?id=", "");
        mail_url_id = mail_url_id.replace("&uid=601116EB-5C67-1EE0-9C04-FBFD28DDAD39", "");
        Log.e("", "Id: "+mail_url_id);
        
        mail_url_uid = mail_url_uid.replace("http://multistore.verifiedwork.com/webservice/changepwd.php?id=2&uid=", "");
        Log.e("", "UId: "+mail_url_uid);
        
        new_pass.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (new_pass.getText().length()>=6) {
						new_pass.setError(null);
					} else if(new_pass.getText().length()>=15) {
						new_pass.setError("Password should be maximum 15 characters");
					}else{
						new_pass.setError("Password should be minimum 6 characters");
					}
				}
			}
		});
		
        confirm_new_pass.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					
					if (new_pass.getText().toString().equals(confirm_new_pass.getText().toString())) {
						confirm_new_pass.setError(null);
					}else{
						confirm_new_pass.setError("Password doesn't match");
					}
				}
			}
		});
        
            
        confirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(fp_validation()){
					if(RegisterActivity.isInternetConnected(getApplicationContext()))
					{
					new ForgotPasswordChangepass().execute();
					}else{
						NoInternet.internetConnectionFinder(getApplicationContext());
					}
					}
			
			}
		});
		
	}
	
	public boolean fp_validation(){
		
		if(new_pass.getText().toString().trim().equalsIgnoreCase("") || confirm_new_pass.getText().toString().trim().equalsIgnoreCase("")
			|| new_pass.getText().toString().length()<6	|| new_pass.getText().toString().length()>15 || 
			confirm_new_pass.getText().toString().length()<6 || confirm_new_pass.getText().toString().length()>15){
			
			if(new_pass.getText().toString().trim().equalsIgnoreCase("")){
				showToast("password is blank");
			}
			if(confirm_new_pass.getText().toString().trim().equalsIgnoreCase("")){
				showToast("conform password is blank");
			}
			if(!new_pass.getText().toString().trim().equals(confirm_new_pass.getText().toString().trim())){
				showToast("passwords mismatch");
			}
			if(new_pass.getText().toString().length()<6	|| new_pass.getText().toString().length()>15){
				showToast("password should be 6 to 15 characters");
			}
			if(confirm_new_pass.getText().toString().length()<6 || confirm_new_pass.getText().toString().length()>15){
				showToast("password character limit exceeds");
			}
			return false;
			
		}else{
			return true;
		}
		
		}
	
	public void showToast(String message){
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
	}
	
	class ForgotPasswordChangepass extends AsyncTask<String, String, String> {

     @Override
     protected void onPreExecute() {
         super.onPreExecute();
         pDialog = new ProgressDialog(ForgotPasswordActivity.this);
         pDialog.setMessage("Reseting password...");
         pDialog.setIndeterminate(false);
         pDialog.setCancelable(false);
         pDialog.show();
     }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
         int success;
                      
         String password = new_pass.getText().toString();
                 
         try {
             // Building Parameters
             List<NameValuePair> params = new ArrayList<NameValuePair>();
             params.add(new BasicNameValuePair("id", mail_url_id));
             params.add(new BasicNameValuePair("uid", mail_url_uid));
             params.add(new BasicNameValuePair("password", password));
            
            // params.add(new BasicNameValuePair("reg_id", gcm1));

             Log.d("request!", "starting");
             // getting product details by making HTTP request
             ServiceHandler sh = new ServiceHandler();
             
             String json = sh.makeServiceCall(FORGOTPASS_URL, 2, params);

             JSONObject jsonObj = new JSONObject(json);
     
             success = Integer.parseInt(jsonObj.getString("code"));
             if (success == 200) {
             	Log.d("Validation Successfull!", "  " );
 
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
             	return jsonObj.getString("code");
             }else{
          	   
             	Log.d("Login Failure!", jsonObj.getString("errors"));
             	errors = jsonObj.getString("errors");
             	//Toast.makeText(ForgotPasswordActivity.this, jsonObj.getString("errors"), Toast.LENGTH_LONG).show();
             	
             	return null;
          	   }
             	

             
         } catch (JSONException e) {
             e.printStackTrace();
         }

         return null;

		}
		/**
      * After completing background task Dismiss the progress dialog
      * **/
     protected void onPostExecute(String file_url) {
         // dismiss the dialog once product deleted
         pDialog.dismiss();
         if (file_url != null){
           Toast.makeText(ForgotPasswordActivity.this, "Password Changed Successfully", Toast.LENGTH_LONG).show();
         }else{
      	   Toast.makeText(ForgotPasswordActivity.this, errors, Toast.LENGTH_LONG).show();
         }

     }

	}
	
}

package com.ikomet.iklocalstore.activities;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.bean.Stores;
import com.ikomet.iklocalstore.utils.InternetConnection;
import com.ikomet.iklocalstore.utils.NoInternet;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;

public class SplashActivity extends Activity {

	ProgressDialog pDialog;
	private static final String LOGIN_URL = "http://multistore.verifiedwork.com/webservice/logincheck.php";
	ArrayList<Category> categoryDetails;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		Currentlocation cl = new Currentlocation(getApplicationContext());
		
		if(SharedPrefs.canGetSPData(getApplicationContext())){
		
		if(InternetConnection.isInternetConnected(getApplicationContext()) && cl.canGetLocation()){
		
				String lat = String.valueOf(cl.getLatitude());
				String lon = String.valueOf(cl.getLongitude());
				
				if(lat!=null && lon!=null){
					new AttemptLogin(lat,lon).execute();
				}else{
					ShowToasts.showToast(getApplicationContext(), "Problem getting location");
				}

				
		}else if(cl.canGetLocation()){
			NoInternet.internetConnectionFinder(getApplicationContext());
		}else{
			ShowToasts.showToast(getApplicationContext(), "Please enable GPS");
		}
		}else{
			ShowToasts.showToast(getApplicationContext(), "Value not in shared Preference");
			
			Handler handler = new Handler();
			
			handler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent intent = new Intent(SplashActivity.this,
							LoginActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(intent);
					finish();
					overridePendingTransition(R.anim.right_in, R.anim.left_out);
				}
			}, 3000);
			
			
			
			
		}
	}

	class AttemptLogin extends AsyncTask<String, String, String> {

		String lat,lon;
		
		AttemptLogin(String lat, String lon){
			this.lat = lat;
			this.lon = lon;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(SplashActivity.this);
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
           int success;
          
           String mail = SharedPrefs.getEmail(getApplicationContext());
           String pword = SharedPrefs.getPassword(getApplicationContext());
           
           
         try {
        	 List<NameValuePair> params = new ArrayList<NameValuePair>();
        	
               params.add(new BasicNameValuePair("username", mail));
               params.add(new BasicNameValuePair("password", pword));
               params.add(new BasicNameValuePair("latitude", lat));
               params.add(new BasicNameValuePair("longitude", lon));
              // params.add(new BasicNameValuePair("reg_id", gcm1));
                      
               // getting product details by making HTTP request
               ServiceHandler sh = new ServiceHandler();
               
               String json = sh.makeServiceCall(LOGIN_URL, 2, params);
               Log.e("request!", "starting : JSONResult is "+ json);
               
               if(json!=null)
               { 
               JSONObject jsonObj = new JSONObject(json);
          
               // json success tag
               success = Integer.parseInt(jsonObj.getString("code"));
               if (success == 200) {
               	Log.d("Login Successful!", "  " );
               	
               	categoryDetails = new ArrayList<Category>();
               	
               	String tkn = jsonObj.getString("token");
               	String fnm = jsonObj.getString("firstname");

               	if(!SharedPrefs.canGetSPData(getApplicationContext())){
               	SharedPrefs.writeSP(getApplicationContext(), fnm, mail, tkn, pword);
               	
               	}
               	
               	SharedPrefs.changeToken(getApplicationContext(), tkn);
               	
               	JSONArray jar = jsonObj.getJSONArray("category");
               	
               	Category cat;
               	
               	for(int i=0;i<jar.length();i++){
               		
//               		cat = new Category();
               		
               		JSONObject jobj1 = jar.getJSONObject(i);
               		String cat_nm = jobj1.getString("name");
//               		cat.setName(cat_nm);
               	
               		String cat_image = jobj1.getString("image");
//               		cat.setImage(cat_image);
               		
               		JSONArray str_nm = jobj1.getJSONArray("stores");
               		
               		ArrayList<Stores> storeDetails = new ArrayList<Stores>();
               		for(int j=0;j<str_nm.length();j++){
                       	
                   		JSONObject jobj = str_nm.getJSONObject(j);
                   		
                   String storeid = jobj.getString("storeid");
                   		System.out.println(storeid);
                   		String storename = jobj.getString("storename");
                   		String latitude = jobj.getString("latitude");
                   		String longitude = jobj.getString("longitude");
                   		String storetype = jobj.getString("storetype");
                   		String storelogo = jobj.getString("storelogo");
                   		
                   		storeDetails.add(new Stores(storeid, storename, latitude, longitude, storetype, storelogo));
                   		
                   
                   	
                   	}
               		categoryDetails.add(new Category(cat_nm, cat_image, storeDetails));
               	}
             
                return jsonObj.getString("msg");
               }else{
            	   
               	Log.d("Login Failure!", jsonObj.getString("msg"));
               	return null;
            	   }
               }
               else
               {
            	 return null; // Toast.makeText(getApplicationContext(), "Problem with loging ", 1000).show();
            	
               }
                            
           } catch (JSONException e) {
               e.printStackTrace();
           }

           return null;

		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			if(pDialog.isShowing()){
			 pDialog.dismiss();
			}
			if (file_url != null) {
				ShowToasts.showToast(getApplicationContext(), " "+file_url);
				finish();
               	overridePendingTransition(R.anim.right_in, R.anim.left_out);
				Intent intent = new Intent(SplashActivity.this, MapActivity.class);
				intent.putExtra("StoreDetails", categoryDetails);
				startActivity(intent);
			}else {
				Toast.makeText(getApplicationContext(),
						"Problem Logging in", Toast.LENGTH_LONG)
						.show();
			}

		}

	}
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}*/

}

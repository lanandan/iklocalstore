/*package com.ikomet.iklocalstore.bean;

import android.graphics.Bitmap;

public class Product {

	String product_id, product_name, actual_price, specials;
	int qty_available, qty_added;
	Bitmap product_image;
	
	public Product(){
		
	}
	
	public Product(String product_id, String product_name, String actual_price,
			String specials, Bitmap product_image, int qty_available, int qty_added) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.actual_price = actual_price;
		this.specials = specials;
		this.product_image = product_image;
		this.qty_available = qty_available;
		this.qty_added = qty_added;
	}
	
	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getActual_price() {
		return actual_price;
	}

	public void setActual_price(String actual_price) {
		this.actual_price = actual_price;
	}

	public String getSpecials() {
		return specials;
	}

	public void setSpecials(String specials) {
		this.specials = specials;
	}

	public Bitmap getProduct_image() {
		return product_image;
	}

	public void setProduct_image(Bitmap product_image) {
		this.product_image = product_image;
	}

	public int getQty_available() {
		return qty_available;
	}

	public void setQty_available(int qty_available) {
		this.qty_available = qty_available;
	}

	public int getQty_added() {
		return qty_added;
	}

	public void setQty_added(int qty_added) {
		this.qty_added = qty_added;
	}
	
}
*/
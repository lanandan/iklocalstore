package com.ikomet.iklocalstore.bean;

import java.io.Serializable;

public class ProductSub implements Serializable{

	String subcategoryid, subcategoryname, subimage;

	public ProductSub() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductSub(String subcategoryid, String subcategoryname,
			String subimage) {
		super();
		this.subcategoryid = subcategoryid;
		this.subcategoryname = subcategoryname;
		this.subimage = subimage;
	}

	public String getSubcategoryid() {
		return subcategoryid;
	}

	public void setSubcategoryid(String subcategoryid) {
		this.subcategoryid = subcategoryid;
	}

	public String getSubcategoryname() {
		return subcategoryname;
	}

	public void setSubcategoryname(String subcategoryname) {
		this.subcategoryname = subcategoryname;
	}

	public String getSubimage() {
		return subimage;
	}

	public void setSubimage(String subimage) {
		this.subimage = subimage;
	}

}

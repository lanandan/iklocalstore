package com.ikomet.iklocalstore.bean;

public class Contacts {

	String name, phone, email_id, gender, company_id, emp_position, emp_id, company_name;

	public Contacts() {
		super();
		
	}

	public Contacts(String name, String phone, String email_id, String gender,
			String company_id, String emp_position, String emp_id,
			String company_name) {
		super();
		this.name = name;
		this.phone = phone;
		this.email_id = email_id;
		this.gender = gender;
		this.company_id = company_id;
		this.emp_position = emp_position;
		this.emp_id = emp_id;
		this.company_name = company_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getEmp_position() {
		return emp_position;
	}

	public void setEmp_position(String emp_position) {
		this.emp_position = emp_position;
	}

	public String getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
		
}

package com.ikomet.iklocalstore.bean;

import java.io.Serializable;
import java.util.ArrayList;

import android.graphics.Bitmap;

public class ProductCategory implements Serializable{

	String categoryid, categoryname;
	String image;
	ArrayList<ProductSub> subcategory;

	public ProductCategory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductCategory(String categoryid, String categoryname,
			String image, ArrayList<ProductSub> subcategory) {
		super();
		this.categoryid = categoryid;
		this.categoryname = categoryname;
		this.image = image;
		this.subcategory = subcategory;
	}

	public String getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(String categoryid) {
		this.categoryid = categoryid;
	}

	public String getCategoryname() {
		return categoryname;
	}

	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ArrayList<ProductSub> getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(ArrayList<ProductSub> subcategory) {
		this.subcategory = subcategory;
	}

}

package com.ikomet.iklocalstore.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProductStore implements Serializable{

	String totaladded, storeid, storename, storephno;
	String banner_img[];
	String discount;

	public ProductStore() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductStore(String totaladded, String storeid, String storename,
			String storephno, String[] banner_img, String discount) {
		super();
		this.totaladded = totaladded;
		this.storeid = storeid;
		this.storename = storename;
		this.storephno = storephno;
		this.banner_img = banner_img;
		this.discount = discount;
	}

	public String getTotaladded() {
		return totaladded;
	}

	public void setTotaladded(String totaladded) {
		this.totaladded = totaladded;
	}

	public String getStoreid() {
		return storeid;
	}

	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}

	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}

	public String getStorephno() {
		return storephno;
	}

	public void setStorephno(String storephno) {
		this.storephno = storephno;
	}

	public String[] getBanner_img() {
		return banner_img;
	}

	public void setBanner_img(String[] banner_img) {
		this.banner_img = banner_img;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

}

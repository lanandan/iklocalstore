package com.ikomet.iklocalstore.bean;

import android.graphics.Bitmap;


public class Products {
	
	String prod_nam, prod_detail;
	int numberofprod, total_prc_for_indv;
	String og_price, discounted_price;
	Bitmap product_img;
	
	public Products(String prod_nam, String prod_detail, String og_price,
			String discounted_price, Bitmap product_img,int numberofprod) {
		super();
		this.prod_nam = prod_nam;
		this.prod_detail = prod_detail;
		this.og_price = og_price;
		this.discounted_price = discounted_price;
		this.product_img = product_img;
		this.numberofprod = numberofprod;
//		this.total_prc_for_indv = total_prc_for_indv; 
	}

	public String getProd_nam() {
		return prod_nam;
	}

	public void setProd_nam(String prod_nam) {
		this.prod_nam = prod_nam;
	}

	public String getProd_detail() {
		return prod_detail;
	}

	public void setProd_detail(String prod_detail) {
		this.prod_detail = prod_detail;
	}

	public int getNumberofprod() {
		return numberofprod;
	}

	public void setNumberofprod(int numberofprod) {
		this.numberofprod = numberofprod;
	}

	public String getOg_price() {
		return og_price;
	}

	public void setOg_price(String og_price) {
		this.og_price = og_price;
	}

	public String getDiscounted_price() {
		return discounted_price;
	}

	public void setDiscounted_price(String discounted_price) {
		this.discounted_price = discounted_price;
	}

	public Bitmap getProduct_img() {
		return product_img;
	}

	public void setProduct_img(Bitmap product_img) {
		this.product_img = product_img;
	}
	
	/*public int getTotal_prc_for_indv() {
		return total_prc_for_indv;
	}

	public void setTotal_prc_for_indv(int total_prc_for_indv) {
		this.total_prc_for_indv = total_prc_for_indv;
	}*/
	
	}

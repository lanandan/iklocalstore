package com.ikomet.iklocalstore.bean;

import android.graphics.Bitmap;

public class ImageBean {

	Bitmap bm;

	public ImageBean(Bitmap bm) {
		super();
		this.bm = bm;
	}

	public Bitmap getBm() {
		return bm;
	}

	public void setBm(Bitmap bm) {
		this.bm = bm;
	}
		
}

package com.ikomet.iklocalstore.bean;

public class ItemProducts {

	String productid, productname, productdesc, quantity, qtyadded, image;
	String actualprice, specials, discounts, attributes, options, images, reviews;
	
	public ItemProducts() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemProducts(String productid, String productname,
			String productdesc, String quantity, String qtyadded, String image,
			String actualprice, String specials, String discounts,
			String attributes, String options, String images, String reviews) {
		super();
		this.productid = productid;
		this.productname = productname;
		this.productdesc = productdesc;
		this.quantity = quantity;
		this.qtyadded = qtyadded;
		this.image = image;
		this.actualprice = actualprice;
		this.specials = specials;
		this.discounts = discounts;
		this.attributes = attributes;
		this.options = options;
		this.images = images;
		this.reviews = reviews;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getProductdesc() {
		return productdesc;
	}

	public void setProductdesc(String productdesc) {
		this.productdesc = productdesc;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getQtyadded() {
		return qtyadded;
	}

	public void setQtyadded(String qtyadded) {
		this.qtyadded = qtyadded;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getActualprice() {
		return actualprice;
	}

	public void setActualprice(String actualprice) {
		this.actualprice = actualprice;
	}

	public String getSpecials() {
		return specials;
	}

	public void setSpecials(String specials) {
		this.specials = specials;
	}

	public String getDiscounts() {
		return discounts;
	}

	public void setDiscounts(String discounts) {
		this.discounts = discounts;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}
	
}

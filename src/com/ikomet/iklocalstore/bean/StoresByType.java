package com.ikomet.iklocalstore.bean;

public class StoresByType {

	String storeid, storename, latitude, longitude, storelogo, distance, scode,
			smsg;

	public StoresByType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StoresByType(String storeid, String storename, String latitude,
			String longitude, String storelogo, String distance, String scode,
			String smsg) {
		super();
		this.storeid = storeid;
		this.storename = storename;
		this.latitude = latitude;
		this.longitude = longitude;
		this.storelogo = storelogo;
		this.distance = distance;
		this.scode = scode;
		this.smsg = smsg;
	}

	public String getStoreid() {
		return storeid;
	}

	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}

	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStorelogo() {
		return storelogo;
	}

	public void setStorelogo(String storelogo) {
		this.storelogo = storelogo;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getScode() {
		return scode;
	}

	public void setScode(String scode) {
		this.scode = scode;
	}

	public String getSmsg() {
		return smsg;
	}

	public void setSmsg(String smsg) {
		this.smsg = smsg;
	}

}

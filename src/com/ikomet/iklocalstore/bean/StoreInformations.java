package com.ikomet.iklocalstore.bean;

public class StoreInformations {

 	String storeid, storename, latitude, longitude, storetype;

	public StoreInformations(){
		
	}
	
	public StoreInformations(String storeid, String storename, String latitude,
			String longitude, String storetype) {
		super();
		this.storeid = storeid;
		this.storename = storename;
		this.latitude = latitude;
		this.longitude = longitude;
		this.storetype = storetype;
	}

	public String getStoreid() {
		return storeid;
	}

	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}

	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStoretype() {
		return storetype;
	}

	public void setStoretype(String storetype) {
		this.storetype = storetype;
	}
	
	
	
	
}

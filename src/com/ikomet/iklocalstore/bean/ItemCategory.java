package com.ikomet.iklocalstore.bean;

import java.util.ArrayList;

public class ItemCategory {

	String categoryid, categoryname;
	ArrayList<ItemProducts> itemProd;

	public ItemCategory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemCategory(String categoryid, String categoryname,
			ArrayList<ItemProducts> itemProd) {
		super();
		this.categoryid = categoryid;
		this.categoryname = categoryname;
		this.itemProd = itemProd;
	}

	public String getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(String categoryid) {
		this.categoryid = categoryid;
	}

	public String getCategoryname() {
		return categoryname;
	}

	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	public ArrayList<ItemProducts> getItemProd() {
		return itemProd;
	}

	public void setItemProd(ArrayList<ItemProducts> itemProd) {
		this.itemProd = itemProd;
	}

}

package com.ikomet.iklocalstore.bean;

import java.io.Serializable;

public class Stores implements Serializable{

	String storeid, storename, latitude, longitude, storetype, storelogo;

	public Stores() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stores(String storeid, String storename, String latitude,
			String longitude, String storetype, String storelogo) {
		super();
		this.storeid = storeid;
		this.storename = storename;
		this.latitude = latitude;
		this.longitude = longitude;
		this.storetype = storetype;
		this.storelogo = storelogo;
	}

	public String getStoreid() {
		return storeid;
	}

	public void setStoreid(String storeid) {
		this.storeid = storeid;
	}

	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getStoretype() {
		return storetype;
	}

	public void setStoretype(String storetype) {
		this.storetype = storetype;
	}

	public String getStorelogo() {
		return storelogo;
	}

	public void setStorelogo(String storelogo) {
		this.storelogo = storelogo;
	}

}

package com.ikomet.iklocalstore.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class Category implements Serializable {

	String name, image;
	ArrayList<Stores> storeDetails;

	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Category(String name, String image, ArrayList<Stores> storeDetails) {
		super();
		this.name = name;
		this.image = image;
		this.storeDetails = storeDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ArrayList<Stores> getStoreDetails() {
		return storeDetails;
	}

	public void setStoreDetails(ArrayList<Stores> storeDetails) {
		this.storeDetails = storeDetails;
	}

}

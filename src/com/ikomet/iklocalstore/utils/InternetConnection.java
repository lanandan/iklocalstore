package com.ikomet.iklocalstore.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnection {

	public static boolean isInternetConnected(Context ctx) {
		ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connectivityMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		if (wifi != null) {
			if (wifi.isConnected()) {
				return true;
			}
		}
		if (mobile != null) {
			if (mobile.isConnected()) {
				return true;
			}
		}
		return false;
	}
	
}

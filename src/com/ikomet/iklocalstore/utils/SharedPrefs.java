package com.ikomet.iklocalstore.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPrefs {

    public static final String EMAIL = "email";
    public static final String TOKEN = "token";
    public static final String UNAME = "username";
    public static final String PASS = "password";
    
    public static final String CHECK_STORE = "checkstore";
    
    public static SharedPreferences prefLogin;
	public static final String MyPREFERENCES = "MyPrefs";
    
    
    public static void writeSP(Context context, String username, String email, String token, String password){
     Log.e("sharedpref", "values are :"+username+email+token+password);
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
       	 Editor editor = prefLogin.edit();
       	 editor.putString(UNAME, username);
         editor.putString(EMAIL, email);
       	 editor.putString(TOKEN, token);
       	 editor.putString(PASS, password);
         editor.commit();
        
    }
	
    public static String getSP(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String username = prefLogin.getString(UNAME, "");
    	String email = prefLogin.getString(EMAIL, "");
    	String token = prefLogin.getString(TOKEN, "");
    	String password = prefLogin.getString(PASS, "");
    	 Log.e("sharedretr", "Ret values are :"+username+email+token+password);
    	/*String username = "visalatchi";
    	String email = "visalatchis.m@gmail.com";
    	String token = "3215656648686564";*/
    	
    	if(!username.equalsIgnoreCase("")&&!email.equalsIgnoreCase("")&&!token.equalsIgnoreCase("")&&!password.equalsIgnoreCase("")){
    	
    	return username+"/"+email+"/"+token+"/"+password;
    	
    	}else{
    		
    		return null;
    	}
//    	return email;
//    	return token;
    }
    
    public static boolean canGetSPData(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String username = prefLogin.getString(UNAME, "");
    	String email = prefLogin.getString(EMAIL, "");
    	String token = prefLogin.getString(TOKEN, "");
    	String password = prefLogin.getString(PASS, "");
    	 Log.e("sharedretr", "Ret values are :"+username+email+token+password);
    	/*String username = "visalatchi";
    	String email = "visalatchis.m@gmail.com";
    	String token = "3215656648686564";*/
    	
    	if(!username.equalsIgnoreCase("")&&!email.equalsIgnoreCase("")&&!token.equalsIgnoreCase("")&&!password.equalsIgnoreCase("")){
    	
    	return true;
    	
    	}else{
    		
    		return false;
    	}

    }
    
    public static String getEmail(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String email = prefLogin.getString(EMAIL, "");
    
    	if(!email.equalsIgnoreCase("")){
    	
    	return email;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getToken(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String token = prefLogin.getString(TOKEN, "");
    
    	if(!token.equalsIgnoreCase("")){
    	
    	return token;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getUname(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String username = prefLogin.getString(UNAME, "");
    	    	
    	if(!username.equalsIgnoreCase("")){
    	
    	return username;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static String getPassword(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	String password = prefLogin.getString(PASS, "");
    	    	    	
    	if(!password.equalsIgnoreCase("")){
    	
    	return password;
    	
    	}else{
    		
    		return null;
    	}
    }
    
    public static void changeToken(Context context, String token){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.putString(TOKEN, token);
    	editor.commit();
    	 Log.e("New token", "New token is :"+token);
    	
    }
    
    public static void clearSP(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.clear();
    	editor.commit();
    }
    
    public static void updateStorecheck(Context context, boolean value){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,context.MODE_PRIVATE);
    	Editor editor = prefLogin.edit();
    	editor.clear();
    	editor.commit();
    }
    
    public static boolean getStorecheck(Context context){
    	prefLogin=context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
    	boolean value = prefLogin.getBoolean(CHECK_STORE, false);
    	return value;
    }
    
}

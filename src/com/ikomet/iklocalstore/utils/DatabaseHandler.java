package com.ikomet.iklocalstore.utils;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ikomet.iklocalstore.bean.StoreInformations;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "ikShop";

	// Contacts table name
	private static final String TABLE_RECENT_STORES = "recentstores";
	
	public static final String KEY_STOREID = "st_id";
	public static final String KEY_STORENAME = "st_nm";
	public static final String KEY_LAT = "latitude";
	public static final String KEY_LONG = "longitude";
	public static final String KEY_STORETYPE = "store_type";
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
				
		String CREATE_RECENTSTORES_TABLE = "CREATE TABLE " + TABLE_RECENT_STORES + "("
				+ KEY_STOREID + " INTEGER NOT NULL,"
				+ KEY_STORENAME + " TEXT,"
				+ KEY_LAT + " TEXT,"
				+ KEY_LONG + " TEXT,"
				+ KEY_STORETYPE + " TEXT" + ")";
				
		/*String CREATE_RECENTSTORES_TABLE = "CREATE TABLE " + TABLE_RECENT_STORES + "("
				+ KEY_STOREID + " INTEGER NOT NULL,"
				+ KEY_STORENAME + " TEXT,"
				+ KEY_LAT + " TEXT,"
				+ KEY_LONG + " TEXT,"
				+ KEY_STORETYPE + " TEXT" + ")";*/
				
		db.execSQL(CREATE_RECENTSTORES_TABLE);
				
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_STORES);
				
		// Create tables agains
		onCreate(db);
	}

	/*
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */
	
	public void addRecentStores(StoreInformations store){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
						
		values.put(KEY_STOREID, Integer.parseInt(store.getStoreid()));
		values.put(KEY_STORENAME, store.getStorename());
		values.put(KEY_LAT, store.getLatitude());
		values.put(KEY_LONG, store.getLongitude());
		values.put(KEY_STORETYPE, store.getStoretype());
				
		db.insert(TABLE_RECENT_STORES, null, values);
	    db.close();
		
	}
	
	public ArrayList<StoreInformations> getRecentStores() {
		ArrayList<StoreInformations> recentStoreList = new ArrayList<StoreInformations>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_RECENT_STORES;
		Log.e("Entered Data read", "Entered Data read");
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				StoreInformations storeinfo = new StoreInformations();
				storeinfo.setStoreid(String.valueOf(cursor.getInt(0)));
				storeinfo.setStorename(cursor.getString(1));
				storeinfo.setLatitude(cursor.getString(2));
				storeinfo.setLongitude(cursor.getString(3));
				storeinfo.setStoretype(cursor.getString(4));
				// Adding categories to list
				recentStoreList.add(storeinfo);
			} while (cursor.moveToNext());
		}
		Log.e("Data read finished", "Data read finished");
		
		db.close();
		// return category list
		return recentStoreList;
	}
	
}

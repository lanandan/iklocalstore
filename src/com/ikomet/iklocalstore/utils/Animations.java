package com.ikomet.iklocalstore.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public class Animations {
	
protected void moveRightToLeft(View leftout,View rightin)
	{
  Animation outtoLeft = new TranslateAnimation(
			Animation.RELATIVE_TO_PARENT,0.0f, Animation.RELATIVE_TO_PARENT,-1.0f,
			Animation.RELATIVE_TO_PARENT,0.0f, Animation.RELATIVE_TO_PARENT,0.0f
			);/*  <---*/
  
  Animation inFromRight = new TranslateAnimation(
		Animation.RELATIVE_TO_PARENT,1.0f, Animation.RELATIVE_TO_PARENT,0.0f,
		Animation.RELATIVE_TO_PARENT,0.0f, Animation.RELATIVE_TO_PARENT,0.0f
		);/* <---*/
  
  outtoLeft.setDuration(1000);
   inFromRight.setDuration(1000);
   rightin.startAnimation(inFromRight);
   leftout .startAnimation(outtoLeft);
  }
protected void moveLeftToRight(View leftin,View rightout)
{
  Animation inFromLeft = new TranslateAnimation(
			Animation.RELATIVE_TO_PARENT,-1.0f, Animation.RELATIVE_TO_PARENT,0.0f,
			Animation.RELATIVE_TO_PARENT,0.0f, Animation.RELATIVE_TO_PARENT,0.0f
			);/*--->*/
  Animation outtoRight = new TranslateAnimation(
			Animation.RELATIVE_TO_PARENT,0.0f, Animation.RELATIVE_TO_PARENT,+1.0f,
			Animation.RELATIVE_TO_PARENT,0.0f, Animation.RELATIVE_TO_PARENT,0.0f
			);/*--->*/			
  outtoRight.setDuration(1000);
  inFromLeft.setDuration(1000);
  rightout.startAnimation(outtoRight);
  leftin.startAnimation(inFromLeft);

       }
}

package com.ikomet.iklocalstore.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.ikomet.iklocalstore.R;

public class NoInternet {
	public static Context nointernet;
	public static Dialog dialog;

	public static void internetConnectionFinder(Context nointernet) {
		final Dialog alertDialog = new Dialog(nointernet);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		alertDialog.setContentView(R.layout.nointernet);
		alertDialog.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		ImageView imgvwCheckinternet = (ImageView) alertDialog
				.findViewById(R.id.nonet);
		TextView txtDes = (TextView) alertDialog.findViewById(R.id.txt_greeting);

		/*
		 * if(Url.selectLanguage.equalsIgnoreCase("espa�ol")){ txtDes.setText(
		 * "Vaya,\n Usted parece haber salido de la cobertura \n red. Se recomienda consultar los ajustes wifi \n o conexi�n de datos m�viles"
		 * ); }
		 */

		final AnimationDrawable animation_checkinternet = (AnimationDrawable) imgvwCheckinternet
				.getDrawable();
		imgvwCheckinternet.post(new Runnable() {

			@Override
			public void run() {
				animation_checkinternet.start();
			}
		});
		// checkInternetConnectionDialog.setCancelable(false);
		alertDialog.show();
        alertDialog.setCancelable(false);
	}
}

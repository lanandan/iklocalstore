package com.ikomet.iklocalstore.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ikomet.iklocalstore.R;
import com.squareup.picasso.Picasso;

public class ListViewAdapter extends BaseAdapter{

	Context cxt;
	String[] name;
	Bitmap[] image;
	
	public ListViewAdapter(Context cxt, String[] name, Bitmap[] image){
		this.cxt=cxt;
		this.name=name;
		this.image=image;
	}
	
	private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        
    }
	
	 public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	         
	        LayoutInflater mInflater = (LayoutInflater) 
	            cxt.getSystemService(cxt.LAYOUT_INFLATER_SERVICE);
	        if (convertView == null) {
	            convertView = mInflater.inflate(R.layout.list_item, null);
	            holder = new ViewHolder();
	            holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_greeting);
	            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView1);
	            convertView.setTag(holder);
	        }
	        else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        	        
	        holder.txtTitle.setText(name[position]);
	        /*if(holder.imageView != null){
	        	Picasso.with(cxt).load(image[position])
	            .placeholder(R.drawable.two)
	            .error(R.drawable.two)
	            .into(holder.imageView);
	        }*/
	        
	        if(image[position]==null){
	        	holder.imageView.setImageBitmap(BitmapFactory.decodeResource(cxt.getResources(), R.drawable.two));
	        }else{
	        	holder.imageView.setImageBitmap(image[position]);
	        }
	        
	        return convertView;
	    }
	 
	    @Override
	    public int getCount() {     
	        return name.length;
	    }
	 
	    @Override
	    public Object getItem(int position) {
	        return name[position];
	    }
	 
	    @Override
	    public long getItemId(int position) {
	        return name.length;
	    }
	
}

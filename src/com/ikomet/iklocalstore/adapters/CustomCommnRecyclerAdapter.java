package com.ikomet.iklocalstore.adapters;

import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.bean.ImageBean;

public class CustomCommnRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	 // The items to display in your RecyclerView
    private List<Object> items;
    Context context;

    private final int TEXT = 0, IMAGE = 1;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomCommnRecyclerAdapter(Context context, List<Object> items) {
    	this.context = context;
        this.items = items;
    }
	
	public CustomCommnRecyclerAdapter(){
		
	}
	
	/*public CustomCommnRecyclerAdapter(){
		
	}*/
	
	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return this.items.size();
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		if (items.get(position) instanceof String) {
	          return TEXT;
	      } else if (items.get(position) instanceof ImageBean) {
	          return IMAGE;
	      }
	      return 0;
		}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
		// TODO Auto-generated method stub
		switch (viewHolder.getItemViewType()) {
        case TEXT:
            ViewHolder1 vh1 = (ViewHolder1) viewHolder;
            configureViewHolder1(vh1, position);
            break;
        case IMAGE:
            ViewHolder2 vh2 = (ViewHolder2) viewHolder;
            configureViewHolder2(vh2, position);
            break;
        default:
//            RecyclerViewSimpleTextViewHolder vh = (RecyclerViewSimpleTextViewHolder) viewHolder;
//            configureDefaultViewHolder(vh, position);
            break;
    }
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		// TODO Auto-generated method stub
		RecyclerView.ViewHolder viewHolder = null;
	      LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

	      switch (viewType) {
	          case TEXT:
	              View v1 = inflater.inflate(R.layout.comm_container_text, viewGroup, false);
	              viewHolder = new ViewHolder1(v1);
	              break;
	          case IMAGE:
	              View v2 = inflater.inflate(R.layout.comm_container_image, viewGroup, false);
	              viewHolder = new ViewHolder2(v2);
	              break;
	          default:
	        	  View v = inflater.inflate(R.layout.comm_container_text, viewGroup, false);
	              viewHolder = new ViewHolder1(v);
	              break;
	      }
	      return viewHolder;
	}
 
	public class ViewHolder1 extends RecyclerView.ViewHolder {

	    private TextView label1;

	    public ViewHolder1(View v) {
	        super(v);
	        label1 = (TextView) v.findViewById(R.id.txt_commhub);
	        label1.setGravity(Gravity.END);
	    }

	    public TextView getLabel1() {
	        return label1;
	    }

	    public void setLabel1(TextView label1) {
	        this.label1 = label1;
	    }
	   
	}
	
	public class ViewHolder2 extends RecyclerView.ViewHolder {

	    private ImageView img1;

	    public ViewHolder2(View v) {
	        super(v);
	        img1 = (ImageView) v.findViewById(R.id.img_commhub);
	        
	    }

	    public ImageView getImg() {
	        return img1;
	    }

	    public void setLabel1(ImageView img1) {
	        this.img1 = img1;
	    }
	   
	}
	
	private void configureViewHolder1(ViewHolder1 vh1, int position) {
	     /* User user = (User) items.get(position);
	      if (user != null) {
	          vh1.getLabel1().setText("Name: " + user.name);
	          vh1.getLabel2().setText("Hometown: " + user.hometown);
	      }*/
		vh1.getLabel1().setText(items.get(position).toString());
		vh1.getLabel1().setGravity(Gravity.END);
		
	  }

	  private void configureViewHolder2(ViewHolder2 vh2, int position) {
		  ImageBean imb = (ImageBean) items.get(position);
		  
		  if(imb != null) {
			  vh2.getImg().setImageBitmap(imb.getBm());
			  
		  }
		  
	   }
	
}

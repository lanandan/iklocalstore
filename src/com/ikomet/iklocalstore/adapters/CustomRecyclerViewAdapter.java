package com.ikomet.iklocalstore.adapters;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.ecommerce.Product;
import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.activities.MapActivity;
import com.ikomet.iklocalstore.bean.ItemProducts;
import com.ikomet.iklocalstore.fragments.FragmentDrawerNew;
import com.ikomet.iklocalstore.fragments.FragmentProductList;
import com.ikomet.iklocalstore.interfaces.FragmentCommunicator;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;
import com.squareup.picasso.Picasso;

	public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.ViewHolder> {
    //private ItemData[] itemsData;
	Context cxt;
	FragmentProductList fpl;
    private ArrayList<ItemProducts> itemsData;
    String url="http://multistore.verifiedwork.com/webservice/addcart.php";
    
    /*public RecyclerViewAdapter(ItemData[] itemsData) {
        this.itemsData = itemsData;
    }*/
    
    public FragmentCommunicator fragmentCommunicator;    
        
    public CustomRecyclerViewAdapter(Context cxt, ArrayList<ItemProducts> itemsData, FragmentCommunicator fragmentCommunicator) {
    	this.cxt = cxt;
        this.itemsData = itemsData;
//        fpl = new FragmentProductList();
        this.fragmentCommunicator = fragmentCommunicator;
    }
    
    // Create new views (invoked by the layout manager)
    @Override
    public CustomRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_listrow, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        
    	// - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
    	
//    	viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
//    	viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());
    	
    	viewHolder.tProductName.setText(itemsData.get(position).getProductname());
    	//viewHolder.tNoofItemsinCart.setText(String.valueOf(itemsData.get(position).getNumberofprod()));
    	viewHolder.tDiscount.setText(String.valueOf(itemsData.get(position).getActualprice()));
    	
    	if(viewHolder.imgProduct != null){
    		
    		 Picasso.with(cxt).load(itemsData.get(position).getImage())
    		    .placeholder(R.drawable.two)
    		    .error(R.drawable.two)
    		    .into(viewHolder.imgProduct);
    		
    	}
    	
//    	viewHolder.imgProduct.setImageBitmap(itemsData.get(position).getImage());
    	
    	int qty_added = Integer.parseInt(itemsData.get(position).getQtyadded());
    		
    	if(qty_added>0){
    			
    			viewHolder.imgAddtoCart.setVisibility(View.INVISIBLE);
    			viewHolder.imgAddtoCart.setClickable(false);
    			
    			viewHolder.tNoofItemsinCart.setText(String.valueOf(qty_added));
    			viewHolder.tNoofItemsinCart.setVisibility(View.VISIBLE);
    			
    			viewHolder.imgAdd.setVisibility(View.VISIBLE);
    			viewHolder.imgAdd.setClickable(true);
    			
    			viewHolder.imgSub.setVisibility(View.VISIBLE);
    			viewHolder.imgSub.setClickable(true);
    		    		
    	}else{
    		
    		viewHolder.imgAddtoCart.setVisibility(View.VISIBLE);
			viewHolder.imgAddtoCart.setClickable(true);
			
			viewHolder.tNoofItemsinCart.setText(String.valueOf(qty_added));
			viewHolder.tNoofItemsinCart.setVisibility(View.INVISIBLE);
			
			viewHolder.imgAdd.setVisibility(View.INVISIBLE);
			viewHolder.imgAdd.setClickable(false);
			
			viewHolder.imgSub.setVisibility(View.INVISIBLE);
			viewHolder.imgSub.setClickable(false);
    		    		
    	}
    	
    }
    
    // inner class to hold a reference to each item of RecyclerView 
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
       
    	public TextView tProductName, tNoofItemsinCart, tDiscount;
        public ImageView imgProduct, imgAddtoCart, imgAdd, imgSub;
        
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tProductName = (TextView) itemLayoutView.findViewById(R.id.txt_product_name_incart);
            imgProduct = (ImageView) itemLayoutView.findViewById(R.id.img_product);
            tNoofItemsinCart = (TextView) itemLayoutView.findViewById(R.id.txt_noofitems_incart);
            tDiscount = (TextView) itemLayoutView.findViewById(R.id.txt_discounted);
            imgAddtoCart = (ImageView) itemLayoutView.findViewById(R.id.img_addtocart);
            imgAdd = (ImageView) itemLayoutView.findViewById(R.id.img_add_cart);
            imgSub = (ImageView) itemLayoutView.findViewById(R.id.img_sub_cart);
            
            itemLayoutView.setOnClickListener(this);
            imgAddtoCart.setOnClickListener(this);
            imgAdd.setOnClickListener(this);
            imgSub.setOnClickListener(this);
            
        }

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int position = getLayoutPosition();
			String product_id = itemsData.get(position).getProductid();
			int total_quantity = Integer.parseInt(itemsData.get(position).getQuantity());
//			Toast.makeText(cxt, "Working click", 1000).show();
			if(v.getId() == R.id.img_addtocart){
				
				if(total_quantity>0){
					new AddCartTask(position, product_id, "1").execute();
				}else{
					ShowToasts.showToast(cxt, "Product is out of stock");
				}
			
			}else if(v.getId() == R.id.img_add_cart){
				
//				ItemProducts prod = new ItemProducts();
//				prod = itemsData.get(position);
				int val = Integer.parseInt(itemsData.get(position).getQtyadded());
				val=val+1;
				
				if(total_quantity>val){
				
//				prod.setQty_added(val);
				String updated_quantity = String.valueOf(val);
				new AddCartTask(position, product_id, updated_quantity).execute();
//				itemsData.remove(position);
//				itemsData.add(position, prod);
				notifyDataSetChanged();
				}else{
					ShowToasts.showToast(cxt, "Stocks not available");
				}
				
				
			}else if(v.getId() == R.id.img_sub_cart){
				
				ItemProducts prod = new ItemProducts();
				prod = itemsData.get(position);
				int val = Integer.parseInt(itemsData.get(position).getQtyadded());
				val = val-1;
				
				String updated_quantity = String.valueOf(val);
				
				if(val==0){
			
					new AddCartTask(position, product_id, "0").execute();
										
				}else if(val>0){
					new AddCartTask(position, product_id, updated_quantity).execute();
				}
				
				/*FragmentDrawerNew.prodDetails.remove(position);
				FragmentDrawerNew.prodDetails.add(position, prod);
				notifyDataSetChanged();*/
				
			}
		}
       
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }
    
   class AddCartTask extends AsyncTask<String, Void, String> {
	    
		int pos;
		String prod_id, quantity;
		private ProgressDialog pDialog;
		String totalnoofitems;
		
		public AddCartTask(int pos, String prod_id, String quantity){
			this.pos = pos;
			this.prod_id = prod_id;
			this.quantity = quantity;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(cxt);
			pDialog.setMessage("Processing...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
			
		}

		@Override
	    public String doInBackground(String... params) {
	    	
	    	ServiceHandler sh = new ServiceHandler();
			List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("username", SharedPrefs.getEmail(cxt)));
	        param.add(new BasicNameValuePair("token", SharedPrefs.getToken(cxt)));
	        param.add(new BasicNameValuePair("product_id", prod_id));
	        param.add(new BasicNameValuePair("qty", quantity));
	        Log.e("QTY_SENDING", quantity);
	        
			String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, param);
			
			Log.e("Json Value", jsonStr);
			String code = null;
			try {
				JSONObject json_obj = new JSONObject(jsonStr);
				
				code = json_obj.getString("code");
				String msg = json_obj.getString("msg");
				if(code.equals("200")){
					
					totalnoofitems = json_obj.getString("totaladded");
					return totalnoofitems;
				
				}else{
					return null;
				}
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	        return null;
	    }

	    @Override
	    public void onPostExecute(String val) {
	    	
	    	if(pDialog.isShowing()){
				 pDialog.dismiss();
				}
	    	
	    	if(val!=null){
				
				ItemProducts prod = new ItemProducts();
				prod = itemsData.get(pos);
				int totalitems = Integer.parseInt(val);
				prod.setQtyadded(quantity);
				itemsData.remove(pos);
				itemsData.add(pos, prod);
//				fpl = new FragmentProductList();
//				fpl.updateFabValue(String.valueOf(MapActivity.fab));
				if(totalitems<10){
				fragmentCommunicator.passDataToFragment("0"+String.valueOf(totalitems));
				}else{
					fragmentCommunicator.passDataToFragment(String.valueOf(totalitems));
				}
				notifyDataSetChanged();
				
				
			}else{
				Toast.makeText(cxt, "Problem getting service", Toast.LENGTH_SHORT).show();
				}
	    
	}
			
	}
}
package com.ikomet.iklocalstore.fragments;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.activities.Currentlocation;
import com.ikomet.iklocalstore.activities.MainScreenActivity;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.bean.ProductCategory;
import com.ikomet.iklocalstore.bean.ProductStore;
import com.ikomet.iklocalstore.bean.ProductSub;
import com.ikomet.iklocalstore.bean.StoresByType;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;

public class FragmentMap extends Fragment {

	public static GoogleMap googleMap;
	
	public ProgressDialog pDialog;

	Bitmap mapmarker = null;

	public static final String MAP_URL = "http://multistore.verifiedwork.com/webservice/storesbytype.php";
	MarkerOptions marker;

	public static ArrayList<String> exp_title;
	public static HashMap<String, ArrayList<String>> exp_store_items;

	String url = "http://multistore.verifiedwork.com/webservice/category.php";

	ArrayList<Category> storeDetails;
	String storetype;
	ArrayList<StoresByType> storeViseList;
	
	ArrayList<ProductCategory> productCatList;
	ProductStore prodStore;

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	public FragmentMap() {

	}

	public FragmentMap(ArrayList<Category> storeDetails) {
		this.storeDetails = storeDetails;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflating view layout
		View layout = inflater.inflate(R.layout.fragment_map, container, false);

		Toast.makeText(getActivity(), "onCreate", 2000).show();
		try {
			// Loading map
			initilizeMap();

			// Changing map type
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

			// Showing / hiding your current location
			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);

			posMarkers();

			googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

						@Override
						public void onInfoWindowClick(Marker arg0) {
							// TODO Auto-generated method stub
							String storename = null, storeid = null;
							// new StoreProductDetails().execute();
							/*for (int i = 0; i < storeDetails.size(); i++) {
								for (int j = 0; j < storeDetails.get(i)
										.getStoreDetails().size(); j++) {
									storename = storeDetails.get(i)
											.getStoreDetails().get(j)
											.getStorename();
									if (storename.equals(arg0.getTitle())) {
										storeid = storeDetails.get(i)
												.getStoreDetails().get(j)
												.getStoreid();
										break;
									}

								}
								if (storename.equals(arg0.getTitle())) {

									break;
								}

							}*/
							
//							new StoreDetails(getActivity(), storeid).execute();
							new StoreDetails(getActivity(), "11").execute();
							
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
		}

		return layout;

	}

	public void searchStore(String storeName) {
		
		String stNam = null;
				
		for(int i=0;i<storeDetails.size();i++){
			for(int j=0;j<storeDetails.get(i).getStoreDetails().size();j++){
				
					stNam = storeDetails.get(i).getStoreDetails().get(j).getStorename();
					if(storeName.equalsIgnoreCase(stNam)){
						googleMap.clear();
						marker = new MarkerOptions();
						
						double lat = Double.valueOf(storeDetails.get(i)
								.getStoreDetails().get(j).getLatitude());
						double lon = Double.valueOf(storeDetails.get(i)
								.getStoreDetails().get(j).getLongitude());
						String storename = storeDetails.get(i).getStoreDetails().get(j)
								.getStorename();
						String storetype = storeDetails.get(i).getStoreDetails().get(j)
								.getStoretype();

						marker.position(new LatLng(lat, lon))
								.title(storename)
								.snippet(storetype)
								.icon(BitmapDescriptorFactory
										.fromResource(R.drawable.bag_1));

							CameraPosition cameraPosition = new CameraPosition.Builder()
									.target(new LatLng(lat, lon)).zoom(18).build();

							googleMap.animateCamera(CameraUpdateFactory
									.newCameraPosition(cameraPosition));
						
						googleMap.addMarker(marker);
						
						break;
					}
				
			}
			
			if(storeName.equals(stNam)){
				
				break;
			}
			
		}
		
	}
	
	public void posMarkers() {
		googleMap.clear();
		for (int i = 0; i < storeDetails.size(); i++) {
			for (int j = 0; j < storeDetails.get(i).getStoreDetails().size(); j++) {

				marker = new MarkerOptions();
				double lat = Double.valueOf(storeDetails.get(i)
						.getStoreDetails().get(j).getLatitude());
				double lon = Double.valueOf(storeDetails.get(i)
						.getStoreDetails().get(j).getLongitude());
				String storename = storeDetails.get(i).getStoreDetails().get(j)
						.getStorename();
				String storetype = storeDetails.get(i).getStoreDetails().get(j)
						.getStoretype();

				marker.position(new LatLng(lat, lon))
						.title(storename)
						.snippet(storetype)
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.bag_1));

				if (i == 0) {

					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(new LatLng(lat, lon)).zoom(18).build();

					googleMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
				}

				googleMap.addMarker(marker);

			}
		}

	}

	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getActivity().getFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getActivity().getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	public class MarkerRePosition extends AsyncTask<String, String, String> {

		String lat, lon;
		Context cxt;

		public MarkerRePosition(Context cxt, String lat, String lon) {
			// TODO Auto-generated constructor stub
			this.cxt = cxt;
			this.lat = lat;
			this.lon = lon;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(cxt);
			pDialog.setMessage("Getting Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag
			try {

				List<NameValuePair> params = new ArrayList<NameValuePair>();

				String email = SharedPrefs.getEmail(cxt);
				String token = SharedPrefs.getToken(cxt);

				params.add(new BasicNameValuePair("username", email));
				params.add(new BasicNameValuePair("token", token));
				params.add(new BasicNameValuePair("storetype", storetype));
				params.add(new BasicNameValuePair("latitude", lat));
				params.add(new BasicNameValuePair("longitude", lon));

				ServiceHandler sh = new ServiceHandler();
				String jsonStr = sh.makeServiceCall(MAP_URL, 2, params);

				JSONObject jobj = new JSONObject(jsonStr);

				Log.e("test", jsonStr);

				int code = Integer.parseInt(jobj.getString("code"));
				String msg = jobj.getString("msg");

				if (code == 200) {
					
					storeViseList = new ArrayList<StoresByType>();
					JSONArray jar = jobj.getJSONArray("stores");

					JSONObject jobj1;

					for (int i = 0; i < jar.length(); i++) {

						jobj1 = jar.getJSONObject(i);

						String storeid, storename, latitude, longitude, storelogo, distance, scode, smsg;

						storeid = jobj1.getString("storeid");
						storename = jobj1.getString("storename");
						latitude = jobj1.getString("latitude");
						longitude = jobj1.getString("longitude");
						storelogo = jobj1.getString("storelogo");
						distance = jobj1.getString("distance");
						scode = jobj1.getString("scode");
						smsg = jobj1.getString("smsg");

						storeViseList.add(new StoresByType(storeid, storename,
								latitude, longitude, storelogo, distance,
								scode, smsg));
											
					}

					String imageurl = jobj.getString("categoryimage");

					InputStream in = new java.net.URL(imageurl).openStream();
					mapmarker = BitmapFactory.decodeStream(in);
					return msg;

				} else {

					return msg;
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// Log.e("Error", e.getMessage());
				e.printStackTrace();
			}

			return null;

		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			if (pDialog.isShowing()) {
				pDialog.dismiss();
			}
			if (file_url != null) {

				marker = new MarkerOptions();
				// initilizeMap();
				googleMap.clear();

				if (storeViseList != null) {
					Log.e("test", ""+storeViseList.size());
					for (int i = 0; i < storeViseList.size(); i++) {

						// marker = new MarkerOptions();
						double lat = Double.valueOf(storeViseList.get(i)
								.getLatitude());
						double lon = Double.valueOf(storeViseList.get(i)
								.getLongitude());
						String storename = storeViseList.get(i).getStorename();

						String scode = storeViseList.get(i).getScode();

						Log.e("test", "Store name = "+storename);
						
						marker.position(new LatLng(lat, lon))
						.title(storename)
						.snippet(storetype)
						.icon(BitmapDescriptorFactory
								.fromBitmap(mapmarker));
						
						if (scode.equals("200")) {

							CameraPosition cameraPosition = new CameraPosition.Builder()
									.target(new LatLng(lat, lon)).zoom(23)
									.build();

							googleMap.animateCamera(CameraUpdateFactory
									.newCameraPosition(cameraPosition));

							// Instantiates a new CircleOptions object +
							// center/radius
							CircleOptions circleOptions = new CircleOptions()
									.center(new LatLng(lat, lon))
									// .center( new LatLng(randomLocation[0],
									// randomLocation[1]) )
									.radius(5).fillColor(0x40ff0000)
									.strokeColor(Color.TRANSPARENT)
									.strokeWidth(2);

							// Get back the mutable Circle
							Circle circle = googleMap.addCircle(circleOptions);
							// more operations on the circle...

						} 
						
						googleMap.addMarker(marker);
						
					}
				} else {
					ShowToasts.showToast(getActivity().getApplicationContext(),
							"" + file_url);
				}

			}

		}

	}
	
	class StoreDetails extends AsyncTask<String, String, String> {
    	
		Context cxt;
    	String storeid, msg;
    	
    	StoreDetails(Context context, String storeid){
    		cxt=context;
    		this.storeid = storeid;
    	}
    	
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(cxt);
			pDialog.setMessage("Getting Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			 // Check for success tag
            
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	        
			String token = SharedPrefs.getToken(getActivity());
			String email = SharedPrefs.getEmail(getActivity());
			
			params.add(new BasicNameValuePair("token", token));
	        params.add(new BasicNameValuePair("username", email));
	        params.add(new BasicNameValuePair("storeid", storeid));
	                 			
           ServiceHandler sh = new ServiceHandler();
           String jsonStr = null;
		   jsonStr = sh.makeServiceCall(url, 2, params);
          Log.e("MainScreenActivity", jsonStr);
           
           JSONObject jobj_store;
		
           try {
        	          	   
        	   jobj_store = new JSONObject(jsonStr);
               	   
        	   int code = jobj_store.getInt("code");
        	   msg = jobj_store.getString("msg");
        	   
        	   if(code==200){
        	           		          		   
        	   String totaladded = jobj_store.getString("totaladded");
        	   String storeid = jobj_store.getString("storeid");
        	   String storename = jobj_store.getString("storename");
        	   String storephno = jobj_store.getString("storephno");
        	   
        	   JSONArray jar_banner = jobj_store.getJSONArray("banner");
        	   
        	   String banner_image[] = new String[jar_banner.length()];
        	   
        	   for(int i=0; i<jar_banner.length();i++){
        		   banner_image[i] = jar_banner.getString(i);
        	   }
        	   
        	   String discount = jobj_store.getString("discount");

        	   prodStore = new ProductStore(totaladded, storeid, storename, storephno, banner_image, discount);
        	          	   
        	   JSONArray jar_category = jobj_store.getJSONArray("category");
        	       	   	   
        	   productCatList = new ArrayList<ProductCategory>();
        	   
        	   for(int i=0;i<jar_category.length();i++){
        		   
        		   JSONObject jobj_store_prod = jar_category.getJSONObject(i);
        		   
        		   String categoryid, categoryname, image_url;
//        		   Bitmap image;
        		   
        		   categoryid = jobj_store_prod.getString("categoryid");
        		   categoryname = jobj_store_prod.getString("categoryname");
        		   image_url = jobj_store_prod.getString("image");
        		   
        		   /*if(!image_url.equalsIgnoreCase("null")){
        		   InputStream in = new java.net.URL(image_url).openStream();
                   image = BitmapFactory.decodeStream(in);
        		   }else{
        			   image = null;
        		   }*/
                                      
        		   String test_subcat=jobj_store_prod.getString("subcategory");
        		   Log.e("test_subcat", test_subcat);

        		   String str_subcat = jobj_store_prod.getString("subcategory");
        		   
        		   ArrayList<ProductSub> productSubList = new ArrayList<ProductSub>();
        		   
        		   if(!str_subcat.equals("null")){
        			
        		   JSONArray jar_subcat = jobj_store_prod.getJSONArray("subcategory");
        		   
        		   for(int j=0;j<jar_subcat.length();j++){
        			   
        			   JSONObject jobj_subcat = jar_subcat.getJSONObject(j);
        			   
        			   String subcategoryid = jobj_subcat.getString("subcategoryid");
        			   String subcategoryname = jobj_subcat.getString("subcategoryname");
        			   String subimage = jobj_subcat.getString("subimage");
        			
        			   productSubList.add(new ProductSub(subcategoryid, subcategoryname, subimage));
        			   
        		   }
        		   }else{
        			   productSubList.add(null);
        		   }
        		   
        		   productCatList.add(new ProductCategory(categoryid, categoryname, image_url, productSubList));
        		   
        	   }
        	   //code is 200
        	   return String.valueOf(code);
        	   
        	   }else{
        		   //code is not 500
        		   return String.valueOf(code);
        	   }
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			
			e.printStackTrace();
		}
                                 
           return null;

		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			if(pDialog.isShowing()){
			 pDialog.dismiss();
			}
			
			if (file_url != null) {
					
				ShowToasts.showToast(cxt, ""+msg);
				
				if(file_url.equals("200")){
				Intent intent = new Intent(cxt, MainScreenActivity.class);
				intent.putExtra("productCat", productCatList);
				intent.putExtra("prodStore", prodStore);
			    cxt.startActivity(intent);
				}
				
			}else {
				Toast.makeText(cxt,
						"Problem getting data", Toast.LENGTH_LONG)
						.show();
			}

		}

	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	
	// method executed from 
	public void passValue(Context cxt, String someValue,
			ArrayList<Category> storeCategory) {
		// TODO Auto-generated method stub
		this.storeDetails = storeCategory;
		if (someValue.equalsIgnoreCase("All")) {
			posMarkers();
		} else {
			storetype = someValue;
			Currentlocation cl = new Currentlocation(cxt);
			String lat = String.valueOf(cl.getLatitude());
			String lon = String.valueOf(cl.getLongitude());

			new MarkerRePosition(cxt, lat, lon).execute();
		}

	}
	
}

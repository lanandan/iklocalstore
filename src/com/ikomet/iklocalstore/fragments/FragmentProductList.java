package com.ikomet.iklocalstore.fragments;


import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.activities.MapActivity;
import com.ikomet.iklocalstore.adapters.CustomRecyclerViewAdapter;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.bean.ItemCategory;
import com.ikomet.iklocalstore.bean.ItemProducts;
import com.ikomet.iklocalstore.bean.Products;
import com.ikomet.iklocalstore.interfaces.FragmentCommunicator;
import com.ikomet.iklocalstore.utils.DividerItemDecoration;

public class FragmentProductList extends Fragment implements FragmentCommunicator{

	RecyclerView mRecyclerView;
	TextView fab;
	CustomRecyclerViewAdapter recyclerAdapter;
	String totaladded;
	ArrayList<ItemCategory> itemList;
	ArrayList<ItemProducts> itemProd;
	
	public FragmentProductList(){
		
	}
	
	public FragmentProductList(String totaladded, ArrayList<ItemCategory> itemList){
		this.totaladded = totaladded;
		this.itemList = itemList;
		
		for(int i=0;i<itemList.size();i++){
			
			itemProd = itemList.get(i).getItemProd();
			
		}
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View layout = inflater.inflate(R.layout.fragment_product_list, container, false);
		
	    mRecyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
	    fab = (TextView)layout.findViewById(R.id.fab);
	    
	    fab.setText(totaladded);

	    mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
   	    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
   	    recyclerAdapter = new CustomRecyclerViewAdapter(getActivity(), itemProd, this);
	    mRecyclerView.setAdapter(recyclerAdapter);
	    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
	    mRecyclerView.setHasFixedSize(true);
  
		return layout;
	}
	
	@Override
	public void passDataToFragment(String someValue) {
		// TODO Auto-generated method stub
		fab.setText(someValue);
					
	}
	
}

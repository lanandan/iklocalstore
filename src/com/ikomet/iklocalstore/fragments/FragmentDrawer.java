package com.ikomet.iklocalstore.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.activities.MapActivity;
import com.ikomet.iklocalstore.adapters.ListAdapterSingleText;
import com.ikomet.iklocalstore.bean.Category;
import com.ikomet.iklocalstore.interfaces.FragmentCommunicator;

public class FragmentDrawer extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
        
    RelativeLayout header;
    TextView title;
    TextView name,data,logout;
    ListView storeType;
    ListAdapterSingleText la; 
    FragmentMap frag=null;
    ArrayList<Category> storeDetails;
    ArrayList<String> storeCategory;
    //public FragmentMap communicator;
    View containerView;
    
    
    public FragmentDrawer() {
		// TODO Auto-generated constructor stub
	}
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }
    
    @Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		
	}
      
    public void getStoreDetails(ArrayList<Category> storeDetails){
    	this.storeDetails = storeDetails;
    	storeCategory = new ArrayList<String>();
    	
    	for(int i=0;i<storeDetails.size()+1;i++){
    		if(i==0){
    			storeCategory.add("All");
    		}else{
    			storeCategory.add(storeDetails.get(i-1).getName());
    		}
    	}
    	
  	
    	la = new ListAdapterSingleText(getActivity(), storeCategory);
    	
    	storeType.setAdapter(la);
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        //communicator = (FragmentCommunicator)FragmentDrawer.this;      	
        header = (RelativeLayout)layout.findViewById(R.id.nav_header_container);
        header.setBackgroundResource(R.drawable.iklocalstore_img);
        title = (TextView)layout.findViewById(R.id.txt_title_category);               
        storeType = (ListView)layout.findViewById(R.id.lst_store_types);
        
           storeType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	   
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String storetype = storeType.getItemAtPosition(position).toString();
				frag = new FragmentMap();
				
				// used to pass values to FragmentMap to reposition markers for all categories
				frag.passValue(getActivity(), storetype, storeDetails);
				mDrawerLayout.closeDrawer(containerView);
			}
		
		});   
        
       return layout;
               
            }
    
	
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
    	containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

  }


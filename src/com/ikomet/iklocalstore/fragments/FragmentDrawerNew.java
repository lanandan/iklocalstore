package com.ikomet.iklocalstore.fragments;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.activities.MainScreenActivity;
import com.ikomet.iklocalstore.activities.MapActivity;
import com.ikomet.iklocalstore.adapters.ExpLstAdapterHash;
import com.ikomet.iklocalstore.bean.ItemCategory;
import com.ikomet.iklocalstore.bean.ItemProducts;
import com.ikomet.iklocalstore.bean.ProductCategory;
import com.ikomet.iklocalstore.bean.ProductStore;
import com.ikomet.iklocalstore.utils.ServiceHandler;
import com.ikomet.iklocalstore.utils.SharedPrefs;
import com.ikomet.iklocalstore.utils.ShowToasts;

public class FragmentDrawerNew extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    
    private View containerView;

    
    
    private ExpLstAdapterHash exp;
    private ExpandableListView ExpandList;
    TextView txt_usr;
    RelativeLayout rl_home;
    Fragment frag = null;
    
    ArrayList<String> expandableListTitleA;
    HashMap<String, ArrayList<String>> expandableListDetailA;
    Context cxt;
    String url = "http://multistore.verifiedwork.com/webservice/product.php";
	ProgressDialog pDialog;
    
	ArrayList<ProductCategory> prodCat = new ArrayList<ProductCategory>();
	ProductStore prodStore;
	
	ArrayList<ItemCategory> itemCat;
	
	public FragmentDrawerNew(){
		
	}
	
	public void getProductCatDet(ArrayList<ProductCategory> prodCat, ProductStore prodStore){
    	this.prodCat = prodCat;
    	this.prodStore = prodStore;
    	
    	expandableListTitleA = new ArrayList<String>();
    	expandableListDetailA = new HashMap<String, ArrayList<String>>();
    	    	
    	ArrayList<String> subcatNames;
    	for(int i=0;i<prodCat.size();i++){
    		
    		String catName = prodCat.get(i).getCategoryname();
    		
    		expandableListTitleA.add(catName);
    		
    		 subcatNames= new ArrayList<String>();
    		Log.e("test_size", ""+prodCat.get(i).getSubcategory().size());
    		for(int j=0;j<prodCat.get(i).getSubcategory().size();j++){
    			
    			String subcatName = prodCat.get(i).getSubcategory().get(j).getSubcategoryname();
    			Log.e("test_subname", ""+subcatName);
    			subcatNames.add(subcatName);

    		}
    		    		
    		expandableListDetailA.put(catName, subcatNames);
    		
    	}
    	
    	exp = new ExpLstAdapterHash(getActivity(), expandableListTitleA, expandableListDetailA);

        ExpandList.setAdapter(exp);
       
    }
     
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer_exp, container, false);

        txt_usr = (TextView)layout.findViewById(R.id.txt_greeting);
        txt_usr.setText("Welcome "+SharedPrefs.getUname(getActivity()));
        rl_home = (RelativeLayout)layout.findViewById(R.id.rl_home);
        
        ExpandList = (ExpandableListView) layout.findViewById(R.id.explist);
              
        ExpandList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				/*if(parent.getChildCount()<1){
					//ExpandList.get
					new ProductsList().execute();
				}*/
				return false;
			}
		});
        
        ExpandList.setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub

				/*String a = exp.getChild(groupPosition, childPosition).toString();
				Toast.makeText(getActivity(), a, 2000).show();
;
				new ProductsList().execute();*/
				String storeid = prodStore.getStoreid();
				String categoryid = exp.getChild(groupPosition, childPosition).toString();
				
				ShowToasts.showToast(getActivity(), "str id = "+storeid+"\n cat id = "+categoryid);
				
//				new ProductsList(getActivity(), storeid, categoryid).execute();
				new ProductsList(getActivity(), "11", "108").execute();
				
				return false;
			}
		});
        
        rl_home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                frag = new FragmentMainScreen();
                fragmentTransaction.replace(R.id.container_body, frag);
                fragmentTransaction.commit();
                
                mDrawerLayout.closeDrawer(containerView);
                				
			}
		});
        
        return layout;
               
            }
     
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }
    
    @Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	
	}

	class ProductsList extends AsyncTask<String, String, String> {

		Context context;
		String store_id, category_id;
		String msg, totaladded;
		
		public ProductsList(){
			
		}
		
		public ProductsList(Context context, String store_id, String category_id){
			this.context = context;
			this.store_id = store_id;
			this.category_id = category_id;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(context);
			pDialog.setMessage("Getting Data...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	
			params.add(new BasicNameValuePair("token", SharedPrefs.getToken(context)));
	        params.add(new BasicNameValuePair("username", SharedPrefs.getEmail(context)));
	        params.add(new BasicNameValuePair("storeid", store_id));
	        params.add(new BasicNameValuePair("categoryid", category_id));
	                 			
           ServiceHandler sh = new ServiceHandler();
           String jsonStr = null;
		   jsonStr = sh.makeServiceCall(url, 2, params);
           Log.e("MainScreenActivity", jsonStr);
        
       try {
   			JSONObject jobj_product = new JSONObject(jsonStr);
   			
   			int code = jobj_product.getInt("code");
   			
   			if(code==200){
   			
   			msg = jobj_product.getString("msg");    
   			String totaladded = jobj_product.getString("totaladded");
   			
   			JSONArray jar_cat = jobj_product.getJSONArray("categories");
   			   		   			
   			itemCat = new ArrayList<ItemCategory>();
   			
   			for(int i=0;i<jar_cat.length();i++){
   			
   				JSONObject jobj_cat = jar_cat.getJSONObject(i);
   				
   				String categoryid = jobj_cat.getString("categoryid");
   				String categoryname = jobj_cat.getString("categoryname");
   				ArrayList<ItemProducts> itemProd = new ArrayList<ItemProducts>();
   				
   				JSONArray jar_prod = jobj_cat.getJSONArray("products");
   				
   				for(int j=0;j<jar_prod.length();j++){
   					
   					JSONObject jobj_prod = jar_prod.getJSONObject(j);
   					
   					String productid = jobj_prod.getString("productid");
   					String productname = jobj_prod.getString("productname");
   					String productdesc = jobj_prod.getString("productdesc");
   					String quantity = jobj_prod.getString("quantity");
   					String qtyadded = jobj_prod.getString("qtyadded");
   					String image = jobj_prod.getString("image");
   					String actualprice = jobj_prod.getString("actualprice");
   					String specials = jobj_prod.getString("specials");
   					String discounts = jobj_prod.getString("discounts");
   					String attributes = jobj_prod.getString("attributes");
   					String options = jobj_prod.getString("options");
   					String images = jobj_prod.getString("images");
   					String reviews = jobj_prod.getString("reviews");
   					
   					itemProd.add(new ItemProducts(productid, productname, productdesc, quantity, qtyadded, image, actualprice, specials, discounts, attributes, options, images, reviews)); 					
   					
   				}
   				
   				itemCat.add(new ItemCategory(categoryid, categoryname, itemProd));
   				
   			}
   			
   			
   			
   			// code is 200 
			return String.valueOf(code);
			
   			}else{
   				// code is 500
   				return String.valueOf(code);
   			}
			
           } catch (Exception e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
		return null;
   	    		
   	    }

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			if(pDialog.isShowing()){
			 pDialog.dismiss();
			}
			
			if(file_url!=null){
			
			if(file_url.equals("200")){
						
			FragmentTransaction fragmentTransaction = MainScreenActivity.fragmentManager.beginTransaction();
            frag = new FragmentProductList(totaladded, itemCat);
            fragmentTransaction.add(R.id.container_body, frag);
            MainScreenActivity.fragmentStack.lastElement().onPause();
            fragmentTransaction.hide(MainScreenActivity.fragmentStack.lastElement());
    		MainScreenActivity.fragmentStack.push(frag);
            fragmentTransaction.commit();
            
            mDrawerLayout.closeDrawer(containerView);
			
			}else{
				ShowToasts.showToast(context, msg);				
			}
			
			}else{
				ShowToasts.showToast(context, "Problem getting Data");
			}
	}
	
	}
    
  }

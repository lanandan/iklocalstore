package com.ikomet.iklocalstore.fragments;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.ikomet.iklocalstore.R;
import com.ikomet.iklocalstore.activities.CommunicationHub;
import com.ikomet.iklocalstore.activities.MainScreenActivity;
import com.ikomet.iklocalstore.adapters.ListViewAdapter;
import com.ikomet.iklocalstore.bean.ProductCategory;
import com.ikomet.iklocalstore.bean.ProductStore;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.squareup.picasso.Picasso;

public class FragmentMainScreen extends Fragment{

	ViewFlipper flipper;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	HorizontalListView hlv;
	ListViewAdapter lva;
	GestureDetector detector = new GestureDetector(new SwipeGestureDetector());
	ImageView image,imageDot;
	LinearLayout privacy_policy;
	RelativeLayout communicationHub;
	TextView fab;
	ArrayList<ProductCategory> prodCat;
	ProductStore prodStore;
	Thread thread;
	Fragment frag = null;
	
	public FragmentMainScreen(){
		
	}
	
	public FragmentMainScreen(ArrayList<ProductCategory> prodCat, ProductStore prodStore){
		this.prodCat = prodCat;
		this.prodStore = prodStore;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		 View layout = inflater.inflate(R.layout.fragment_mainscreen, container, false);
		
		 flipper = (ViewFlipper)layout.findViewById(R.id.view_flipper);
		 privacy_policy = (LinearLayout)layout.findViewById(R.id.ll_del);
		 communicationHub = (RelativeLayout)layout.findViewById(R.id.rel_commun_hub);
		 fab = (TextView)layout.findViewById(R.id.fab);
		 
		 fab.setText(prodStore.getTotaladded());
		 
		 communicationHub.setOnClickListener(new View.OnClickListener() {
		 	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), CommunicationHub.class);
				startActivity(intent);
			}
		 });
		 
		 privacy_policy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*FragmentPrivacyPolicy frag = new FragmentPrivacyPolicy();
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	            fragmentTransaction.replace(R.id.container_body, frag);
	            fragmentTransaction.commit();*/
				
				FragmentTransaction fragmentTransaction = MainScreenActivity.fragmentManager.beginTransaction();
	            frag = new FragmentPrivacyPolicy();
	            fragmentTransaction.add(R.id.container_body, frag);
	            MainScreenActivity.fragmentStack.push(frag);
	            fragmentTransaction.commit();
				
			}
		});
		
		hlv = (HorizontalListView)layout.findViewById(R.id.HorizontalListView);
		 
		setFlipperContent();
			
		setHorizontalAdapter();
					
		hlv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					//String val = parent.getItemAtPosition(position).toString();
					
				}
		});
			
			
		flipper.setOnTouchListener(new OnTouchListener() {
		@Override
		public boolean onTouch(final View view, final MotionEvent event) {
				detector.onTouchEvent(event);
		return true;
				}
		});
			
		 
		return layout;
	}

	private void setFlipperContent() {

		for (int i = 0; i < prodStore.getBanner_img().length; i++) {
	        LayoutInflater inflater = (LayoutInflater) getActivity()
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View view = inflater.inflate(R.layout.fliper_container, null);

	        flipper.addView(view);

	        image = (ImageView) view.findViewById(R.id.imgView);
//	        imageDot = (ImageView) view.findViewById(R.id.imgView_dot);
	        if(image!=null){
	        	Picasso.with(getActivity()).load(prodStore.getBanner_img()[i])
	            .placeholder(R.drawable.two)
	            .error(R.drawable.two)
	            .into(image);
	        }
//	        image.setImageBitmap(prodStore.getBanner_img());
//	        imageDot.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ic_action));
	    }
	    //setFlipperAnimation();
		
	}
	
	public void setHorizontalAdapter(){
		
		new imageAsync().execute();
		
	}
	
	class imageAsync extends AsyncTask<String, Void, String> {

		String catNames[] = new String[prodCat.size()];
		String catImagesUrl[] = new String[prodCat.size()];
		Bitmap catImages[] = new Bitmap[prodCat.size()];
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
								
			for(int i=0;i<prodCat.size();i++){
				
				catNames[i] = prodCat.get(i).getCategoryname();
				catImagesUrl[i] = prodCat.get(i).getImage();
				
			}
			
			for(int i=0;i<catImagesUrl.length;i++){
				
				if(!catImagesUrl[i].equalsIgnoreCase("null")){
	        		   InputStream in = null;
					try {
						
						in = new java.net.URL(catImagesUrl[i]).openStream();
					
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        		   catImages[i] = BitmapFactory.decodeStream(in);
	        		   
				}else{
	        			   catImages[i] = null;
	        		 }
				
				}
			
			return "success";
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if(result!=null){
				lva = new ListViewAdapter(getActivity(), catNames, catImages);
				
				hlv.setAdapter(lva);
			}
			
		}
		
	}

	
	class SwipeGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			try {
				// right to left swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					flipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.in_from_right));
					flipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(),R.anim.out_to_left));				
					flipper.showNext();
					return true;
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					flipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.in_from_left));
					flipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.out_to_right));					
					flipper.showPrevious();
					return true;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}
	}
		
}

package com.ikomet.iklocalstore.interfaces;

public interface FragmentCommunicator{
	   public void passDataToFragment(String someValue);
	}
